import tensorflow as tf
import tensorflow_probability as tfp
import math as math
from tensorflow import keras
from tensorflow.keras import layers
#from helper_functions.MIE_NN.Besselyy2 import *
#from helper_functions.MIE_NN.Besseljj2 import *
import numpy as np

import sys


# This function is used to calculate the Mie coefficients a_n and b_n.
# For reference, see the book Bohren & Huffman. The basic solution is given
# in equation (4.53). The version implemented here is from equations
# (4.56) and (4.57), which assume the permeability of particle and
# surrounding medium to be the same!! This is an ASSUMPTION.

# To compute the coefficients we use the equations given in section
# 4.8 of the book.

# Inputs: m is the (complex) refractive index of the particle, x is the
# size parameter calculated as 2 * pi * n * r / lambda, and n is the
# refractive index of the medium (assumed to be 1 by default here), r
# is the particle size (radius) and lambda the wavelength of the incoming light

def get_TFMieCoefficients_ab():

    n = tf.range(1.,51.)
    n = tf.cast(n, tf.complex128)
    n = n[tf.newaxis]

    inumber = tf.cast(tf.complex(0.,1.), tf.complex128)

    @tf.function(experimental_relax_shapes=True)
    def TFMieCoefficients_ab(m,x,batchsize):

        m = tf.cast(m, tf.complex128)
        x = tf.cast(x, tf.complex128)
        
        D_n = TFDn(m,x, batchsize)

    #this will give us the index for the bessel/hankel/etc. functions

        FactorFora_n = D_n/m + tf.transpose(n)/x
        FactorForb_n = m*D_n + tf.transpose(n)/x

        #BesseljyTensor returns two arrays, one with the bessel functions from 0-49 and one from 1-50
        BesseljArray0, BesseljArray = BesseljTensor(x, batchsize)
        BesselyArray0, BesselyArray = BesselyTensor(x, batchsize)

        RiccatiSn = x*BesseljArray
        RiccatiCn = -x*BesselyArray
        RiccatiXin = RiccatiSn - inumber*RiccatiCn

        RiccatiSn0 = x*BesseljArray0
        RiccatiCn0 = -x*BesselyArray0
        RiccatiXin0 = RiccatiSn0 - inumber*RiccatiCn0

        a_n = (FactorFora_n*RiccatiSn - RiccatiSn0) / (FactorFora_n*RiccatiXin-RiccatiXin0)
        b_n = (FactorForb_n*RiccatiSn - RiccatiSn0) / (FactorForb_n*RiccatiXin-RiccatiXin0)

        return a_n, b_n

    return TFMieCoefficients_ab



# Here the angle-dependent functions pi_n and tau_n are calculated
# via a recursion relation as shown in equation (4.47)

# Define a function that gives the logarithmic derivative

#Define a function that gives the logarithmic derivative
@tf.function(experimental_relax_shapes=True)
def TFDn(m,x, batchsize):
    
    x = tf.cast(x, tf.complex128)
    m = tf.cast(m, tf.complex128)
    
    ta = tf.TensorArray(tf.complex128, size=0, dynamic_size=True)
    ta = ta.unstack(tf.zeros((61, batchsize), dtype=tf.complex128))

    for i in range(59, 0,-1):
        ta = ta.write(i, (i+1.)/(m*x) - 1./((ta.read(i + 1)) + (i+1.)/(m*x)))

    output = ta.stack()[1:51]

    return output



def get_pitau():

    pi = tf.constant(np.load('/data/user/ponts_m/aerosol/aerosol_code1/helper_functions/MIE_NN/pistack.npy'))
    tau = tf.constant(np.load('/data/user/ponts_m/aerosol/aerosol_code1/helper_functions/MIE_NN/taustack.npy'))
    
    indextensor = tf.range(1.,51.,1)
    indextensor = tf.cast(indextensor, tf.float64)

    @tf.function(experimental_relax_shapes=True)
    def pitau(sizeparameter, batchsize):
        
        sizeparameter = tf.cast(sizeparameter, tf.float64)
        
        piresult = tf.zeros((180,50), dtype=tf.float64)
        tauresult = tf.zeros((180,50), dtype=tf.float64)

        piresult = piresult[:,:,tf.newaxis]
        tauresult = tauresult[:,:,tf.newaxis]

        piplaceholder = tf.zeros((50,180), dtype=tf.float64)
        tauplaceholder = tf.zeros((50,180), dtype=tf.float64)

        for i in range(batchsize):

            piplaceholder = tf.where(indextensor <= 2+sizeparameter[i]+4*sizeparameter[i]**(1/3), pi, 0)
            tauplaceholder = tf.where(indextensor <= 2+sizeparameter[i]+4*sizeparameter[i]**(1/3), tau, 0)

            piplaceholder2 = piplaceholder[:,:,tf.newaxis]
            tauplaceholder2 = tauplaceholder[:,:,tf.newaxis]

            piresult = tf.concat([piresult, piplaceholder2], 2)
            tauresult = tf.concat([tauresult, tauplaceholder2], 2)


        piresult = piresult[:,:,1:]
        tauresult = tauresult[:,:,1:]

        piresult = tf.cast(piresult, tf.complex128)
        tauresult = tf.cast(tauresult, tf.complex128)

        return tf.transpose(piresult, [1, 0, 2]), tf.transpose(tauresult, [1, 0, 2])

    return pitau


 #Here the Matrix Elements of the Scattering (2x2) matrix are calculated.
 #The formulas for these matrix elements are given in equation (4.74)
 #from these formulas we can then calculate the phase functions in the
 #mueller matrix


def get_TFScatteringMatrixElements():

    n = tf.range(1.,51.)
    n = tf.cast(n, tf.complex128)

    Prefactor = (2.*n+1.)/(n*(n+1.))
    Prefactor = Prefactor[tf.newaxis,tf.newaxis]
    Prefactor = tf.transpose(Prefactor)

    TFMC = get_TFMieCoefficients_ab()

    @tf.function(experimental_relax_shapes=True)
    def TFScatteringMatrixElements(m,x,intsteps):

       #determine the coefficients used to calculate the matrix elements S1 and S2
       #see function description for MieCoefficients_ab above

       a_n, b_n = TFMC(m,x,intsteps)
       a_n = a_n[:,tf.newaxis,:]
       b_n = b_n[:,tf.newaxis,:]

       #Calculate the functions pi and tau needed for the calculation.
       #see function description for PieTau above
       pt = get_pitau()
       pi_n, tau_n = pt(x, intsteps)

       #finally, actual calculation of the sum (4.74) to get the matrix elements S1 and S2 ;)
       S_1 = tf.reduce_sum(Prefactor * ( pi_n * a_n + tau_n * b_n),0)
       S_2 = tf.reduce_sum(Prefactor * ( tau_n * a_n + pi_n * b_n),0)

       #The returned matrices will have the S-values as entries. Since S depends on the angle and the
       #refractive index, we have "two dimensions". The columns will correspond to different refractive
       #indices and the rows will correspond to different angles. so the entry M_57 in the matrix
       #will be the S-element for the angle of 5 degrees of the 7th refractive index in the data set
       return S_1, S_2

    return TFScatteringMatrixElements

   #Here we use equation(s) (4.77) in the book to get the two elements we want
   #we want S11 and -S12/S11, which is the polarized phase function
   #the radius and the wavelength have to be input with the same units. so if one
   #used nanometers for the wavelength, the particle radius needs to be nanometers as well.
def get_TFPhaseFunctions():

    TFSME = get_TFScatteringMatrixElements()

    @tf.function(experimental_relax_shapes=True)
    def TFPhaseFunctions(m,wavelength,radius,intsteps):
        
         pi = tf.cast(math.pi, tf.float64)

         x = 2*pi*radius/wavelength

         S_1, S_2 = TFSME(m,x,intsteps)

         S_11 = 0.5*(tf.abs(S_2)**2+tf.abs(S_1)**2)
         S_12 = 0.5*(tf.abs(S_2)**2-tf.abs(S_1)**2)

         return S_11, S_12

    return TFPhaseFunctions


@tf.function(experimental_relax_shapes=True)
def TFsizedistribution(r, Vtot, sigma, rmean):

    sigma = tf.math.log(sigma)
    pi = tf.cast(math.pi, tf.float64)
    mu = tf.math.log(rmean) - 3*sigma**2
    N0 = 3. / 4. / pi * Vtot * tf.exp(-(3.*mu + 4.5*sigma**2))

    return N0 /(tf.sqrt(2*pi)) * 1/sigma * 1/r * tf.exp(-(tf.math.log(r)-mu)**2/(2*sigma**2))



def get_TFEnsembleCalculation():

    radius = tf.range(50, 15000, 10)
    radius = tf.cast(radius, tf.float64)
    radiusrepeat = tf.transpose(tf.repeat(radius[:,tf.newaxis], 180, axis=1))

    intsteps = 1495

    TFPF = get_TFPhaseFunctions()
    
    zero = tf.constant(0, dtype=tf.float64)
    
    epsilon = tf.constant(1e-100, dtype=tf.float64)

    @tf.function(experimental_relax_shapes=True)
    def TFEnsembleCalculation(m, wavelength, Vtot, sigma, rmean):

        m = tf.cast(m, tf.complex128)
        wavelength = tf.cast(wavelength, dtype = tf.float64)
        Vtot = tf.cast(Vtot, dtype = tf.float64)
        sigma = tf.cast(sigma, dtype = tf.float64)
        rmean = tf.cast(rmean, dtype = tf.float64)

        TFS11, TFS12 = TFPF(m, wavelength, radius, intsteps)

        S11Integrand = TFS11*TFsizedistribution(radius, Vtot, sigma, rmean)
        S12Integrand = TFS12*TFsizedistribution(radius, Vtot, sigma, rmean)

        S11Result = tfp.math.trapz(S11Integrand, x = radiusrepeat)
        S12Result = tfp.math.trapz(S12Integrand, x = radiusrepeat)
        
        S11Result = tf.math.add(S11Result, epsilon)
        
        ppf = tf.math.divide(S12Result, S11Result)
        ppf = tf.subtract(zero, ppf)

        return S11Result, ppf

    return TFEnsembleCalculation


def get_PINNLossFunction():

    TFEC = get_TFEnsembleCalculation()

    @tf.function(experimental_relax_shapes=True)
    def PINNLossFunction(phasefunctions, output, y_true, batchsize):

        n = output[:,0]
        k = output[:,1]

        n = tf.cast(n, dtype=tf.complex128)
        k = tf.cast(k, dtype=tf.complex128)
        k = k*1j

        m = n+k

        Vtot = output[:,4]
        Vtot = tf.cast(Vtot, dtype=tf.float64)

        sigma = output[:,2]
        sigma = tf.cast(sigma, dtype=tf.float64)

        rmean = output[:,3]
        rmean = tf.cast(rmean, dtype=tf.float64)
        

        wavelength = tf.cast(532, dtype = tf.complex128)

        loss = tf.TensorArray(dtype = tf.float64, size=batchsize, dynamic_size=True, clear_after_read=False)
        loss = loss.unstack(tf.zeros((batchsize, 360), dtype=tf.float64))
        
        
        phasefunctions = tf.math.exp(phasefunctions)
        
        print(phasefunctions)
        print(sigma)

        for i in range(batchsize):
            
            print('something is happening', i)

            S11, PPF = TFEC(m[i], wavelength, Vtot[i], sigma[i], rmean[i])
            PPF = tf.math.exp(PPF)

            S11PPF = tf.concat([S11,PPF],0)
            print(S11PPF)

            loss = loss.write(i, S11PPF)

        mse = tf.reduce_mean(tf.square(output-y_true))
        rmse = tf.math.sqrt(mse)
        
        intermediate = tf.subtract(loss.stack(), phasefunctions)
        intermediate = tf.math.square(intermediate)
        intermediate = tf.reduce_mean(intermediate)

        rmsresult = tf.math.sqrt(intermediate)

        return rmsresult + rmse

    return PINNLossFunction


def get_PINNLossFunction1():

    TFEC = get_TFEnsembleCalculation()
    
    @tf.function(experimental_relax_shapes=True)
    def PINNLossFunctionConcat(output, y_true, batchsize):

        phasefunctions = output[:,:-5]

        n = output[:,-5]
        k = output[:,-4]

        n = tf.cast(n, dtype=tf.complex128)
        k = tf.cast(k, dtype=tf.complex128)
        k = k*1j

        m = n+k

        Vtot = output[:,-1]
        Vtot = tf.cast(Vtot, dtype=tf.float64)

        sigma = tf.math.abs(output[:,-3])
        sigma = tf.cast(sigma, dtype=tf.float64)

        rmean = tf.math.abs(output[:,-2])
        rmean = tf.cast(rmean, dtype=tf.float64)

        wavelength = tf.cast(532, dtype = tf.complex128)


        loss = tf.TensorArray(dtype = tf.float64, size=batchsize, dynamic_size=True, clear_after_read=False)
        loss = loss.unstack(tf.zeros((batchsize, 360), dtype=tf.float64))
        

        for i in range(batchsize):

            S11, PPF = TFEC(m[i], wavelength, Vtot[i], sigma[i], rmean[i])

            S11PPF = tf.concat([S11,PPF],0)

            loss = loss.write(i, S11PPF)

        mse = tf.reduce_mean(tf.square(output[:,-5:]-y_true))
        rmse = tf.math.sqrt(mse)
        
        intermediate = tf.subtract(loss.stack(), phasefunctions)
        intermediate = tf.math.square(intermediate)
        intermediate = tf.reduce_mean(intermediate)

        rmsresult = tf.math.sqrt(intermediate)

        return rmsresult + rmse
    
    return PINNLossFunctionConcat

PINNLossFunction = get_PINNLossFunction1()

# #### Figure out how the NN returns the parameters to write the IntegratedEnsembleMatrixFromParameterSet - Function
# using the correct input format #####


