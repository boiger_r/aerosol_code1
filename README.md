


# Retrieval of Aerosol Properties from Measurement Data


This repository contains the code for building neural network models for the
retrieval of aerosol properties from scattering data.

Forward Problem: Given the aerosol properties compute the phase functions
Inverse Problem: Given the phase functions determine the aerosol properties

Implemented so far:
for Forward Problem: surrogate model using dense neural network, Invertible Neural
Network Model, Autoencoder (Encoder)
for Inverse Problem: Invertible Neural NEtwork Model, Autoencoder (Decoder)



