# Python script to create the dataset using pymiescatt
# save command is commented out to not overwrite the files!!!


import argparse
import numpy as np
import pandas as pd
import time
import PyMieScatt as ps
from multiprocessing import Pool
from scipy.stats import lognorm

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--datafile_input', help='datafile input')
parser.add_argument('--datafile_output', help='datafile output')
args = parser.parse_args()

arg_datafile_input = args.datafile_input
arg_datafile_output = args.datafile_output

start = time.time()

def lognormal(D_array, D_median, GSD, Ntot):  # dn/dD
    n = (Ntot/(np.sqrt(2*np.pi)*(np.log(GSD)))/D_array) * np.exp(-(np.log(D_array)-np.log(D_median))**2/(2*(np.log(GSD)**2)))   # nm-1 * cm-3
    return n

def run_pymiescatt(R_median, GSD, N_tot, ri_n, ri_k, wvl, angles, D_array):
    '''
    R_median in um (NOTE: this is the median radius of the NUMBER size distribution!)
    N_tot in cm-3
    ri_k as positive float number
    wvl in um
    angles is the array of angles
    D_array is the diameter array in nm
    '''
    lambd = wvl *1e3   # from um to nm
    m = ri_n + 1j * ri_k
    mu = np.cos(angles * np.pi / 180)
    
    n = lognormal(D_array, R_median*2 * 1e3, GSD, N_tot)     
        
    S11 = []
    S12 = []
    for diam in D_array:
        x = diam * np.pi / lambd
        theta, SL, SR, S11_temp = ps.ScatteringFunction(m, lambd, diam,
                                                        minAngle=angles.min(), maxAngle=angles.max(),
                                                        angularResolution=angles[1]-angles[0],
                                                        normalization=None)
    
        S11.append(S11_temp)
        S12.append(0.5*(SR-SL))
    S11 = np.array(S11).T
    S12 = np.array(S12).T  
            
    F11 = np.trapz(S11 * n * (lambd/(2*np.pi))**2. * 1e-6, x=D_array)
    F12 = np.trapz(S12 * n * (lambd/(2*np.pi))**2. * 1e-6, x=D_array)

#    PPF = - F12 / F11
      
    pymiescatt = np.concatenate((F11,F12)) 
    return pymiescatt


# +
input1 = pd.read_hdf(arg_datafile_input,key='dvar1')

input2 = pd.read_hdf(arg_datafile_input,key='dvar2')


# -

# input1 = input1.iloc[0:6]


name = []
for i in range(180):
    name.append('F11_'+str(i))    
for i in range(180):
    name.append('F12_'+str(i))




angles = np.arange(0,180,1)
wvl =  0.532


def process_input(i):
    
    
    V_tot, R_median_V, GSD, ri_n, ri_k = input1.iloc[i].values
    
    #D_array = np.logspace(0.65, 4.5, 400)  # [nm]

    
    
    inputset = [V_tot,R_median_V,GSD,ri_n,ri_k]
    D_median_V = R_median_V*2  # [nm]
    D_median_N = (np.exp(np.log(D_median_V) - (3*np.log(GSD)**2)))       
    
    
    
    # [nm]
    testnr = 10
    #D_array = np.logspace(np.log10(D_median_N/GSD/10),np.log10(D_median_N*GSD*10),200)
    lower = D_median_V/GSD/testnr
    upper = D_median_V*GSD*testnr

    D_array = np.logspace(np.log10(lower),np.log10(upper),301)

    
    
    
    
    N_tot = 6 / np.pi * V_tot*1e9 / (np.exp((3*np.log(D_median_N)) + (4.5*np.log(GSD)**2))) # [cm^-3]
    datalist1 = run_pymiescatt(D_median_N / 2 * 1e-3, GSD, N_tot, ri_n, ri_k, wvl, angles, D_array)

    return i, inputset, datalist1
p = Pool(88)


def process_input1(i1):
    
    
    V_tot1, R_median_V1, GSD1, ri_n1, ri_k1 = input2.iloc[i1].values
    
    #D_array = np.logspace(0.65, 4.5, 400)  # [nm]

    
    
    inputset1 = [V_tot1,R_median_V1,GSD1,ri_n1,ri_k1]
    D_median_V1 = R_median_V1*2  # [nm]
    D_median_N1 = (np.exp(np.log(D_median_V1) - (3*np.log(GSD1)**2)))       
    
    
    
    # [nm]
    testnr1 = 10
    #D_array = np.logspace(np.log10(D_median_N/GSD/10),np.log10(D_median_N*GSD*10),200)
    lower1 = D_median_V1/GSD1/testnr1
    upper1 = D_median_V1*GSD1*testnr1

    D_array1 = np.logspace(np.log10(lower1),np.log10(upper1),301)

    
    
    
    
    N_tot1 = 6 / np.pi * V_tot1*1e9 / (np.exp((3*np.log(D_median_N1)) + (4.5*np.log(GSD1)**2))) # [cm^-3]
    
    datalist2 = run_pymiescatt(D_median_N1/ 2 * 1e-3, GSD1 ,N_tot1, ri_n1, ri_k1, wvl, angles, D_array1)

    return i1, inputset1, datalist2
p = Pool(88)

results = p.map(process_input, np.arange(len(input1)))
results1 = p.map(process_input1, np.arange(len(input2)))

results_input = pd.DataFrame(columns = ['Vtot','RmedianV','GSD','n','k'])
results_output = pd.DataFrame(columns= name)
for i in range(len(results)):
    print(results[i][0])
    results_input.loc[i]=results[i][1]
    results_output.loc[i]=results[i][2]
results_input1 = pd.DataFrame(columns = ['Vtot','RmedianV','GSD','n','k'])
results_output1 = pd.DataFrame(columns= name)
for i in range(len(results1)):
    print(results1[i][0])
    results_input1.loc[i]=results1[i][1]
    results_output1.loc[i]=results1[i][2]




# +



results_output.to_hdf(arg_datafile_output,key='qoi1')    

results_output1.to_hdf(arg_datafile_output,key='qoi2')    
results_input.to_hdf(arg_datafile_output,key='dvar1')  
results_input1.to_hdf(arg_datafile_output,key='dvar2')  



