import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import streamlit as st
from mllib.model import KerasSurrogate


@st.cache(hash_funcs={KerasSurrogate: lambda s: None})
def load_model():
    return KerasSurrogate.load('../models', 'awa_forward_model')


@st.cache
def load_uncertainties():
    file = 'plots/models/awa_forward_model/forward_prediction_residual_quantiles_by_longitudinal_pos.csv'
    uncertainties = pd.read_csv(file)
    uncertainties.rename(columns={
                            'Unnamed: 1': 'confidence',
                         }, inplace=True)
    return uncertainties


# load the prediction uncertainty
uncertainties = load_uncertainties()
confidence_levels = uncertainties['confidence'].unique()

# get user input and build the input vector for the model
IBF = st.sidebar.number_input('IBF [A]', min_value=450., max_value=550.)
IM = st.sidebar.number_input('IM [A]', min_value=100., max_value=260.)
GPHASE = st.sidebar.number_input('GPHASE [A]', min_value=-50., max_value=10.)
ILS1 = st.sidebar.number_input('ILS1 [A]', min_value=0., max_value=250.)
ILS2 = st.sidebar.number_input('ILS2 [A]', min_value=0., max_value=200.)
ILS3 = st.sidebar.number_input('ILS3 [A]', min_value=0., max_value=200.)
charge = st.sidebar.number_input('Chanrge [nC]', min_value=0.3, max_value=5.)
lamb = st.sidebar.number_input('lambda [ps]', min_value=0.3, max_value=2.)
SIGXY = st.sidebar.number_input('SIGXY [mm]', min_value=1.5, max_value=12.5)
confidence = st.selectbox('Confidence',
                          options=confidence_levels,
                          index=len(confidence_levels)-1)
uncertainties = uncertainties.query('`confidence` == @confidence')
uncertainties.reset_index(drop=True, inplace=True)

n_plot = len(uncertainties['Path length'].unique())
s = uncertainties['Path length'].unique().reshape((n_plot, 1))

x = np.array([
    IBF,
    IM,
    GPHASE,
    ILS1,
    ILS2,
    ILS3,
    charge,
    lamb,
    SIGXY,
]).reshape((1, 9))
x = np.repeat(x, n_plot, axis=0)
x = np.append(x, s, axis=1)

# load the model and predict
surr = load_model()
y = surr.predict(x, batch_size=256)
y = pd.DataFrame(data=y, columns=[
    'Mean Bunch Energy',
    'RMS Beamsize in x',
    'RMS Beamsize in y',
    'Normalized Emittance x',
    'Normalized Emittance y',
    'energy spread of the beam',
    'Correlation xpx',
    'Correlation ypy',
])

y['RMS Beamsize in x'] *= 1000.
y['Normalized Emittance x'] *= 1e6
y['energy spread of the beam'] *= 1000.

# plot the predictions
fig, axes = plt.subplots(3, 2, figsize=(16, 15))

to_plot = [
    'RMS Beamsize in x',
    'Normalized Emittance x',
    'Mean Bunch Energy',
    'energy spread of the beam',
    'Correlation xpx',
]

labels = {
    'RMS Beamsize in x': r'$\sigma_x$ [mm]',
    'Normalized Emittance x': r'$\epsilon_x$ [mm mrad]',
    'Mean Bunch Energy': r'$E$ [MeV]',
    'energy spread of the beam': r'$\Delta E$ [keV]',
    'Correlation xpx': r'$\mathrm{Corr}(x, p_x)$',
}

limits = {
    'RMS Beamsize in x': [0., 15.],
    'Normalized Emittance x': [1., 1000.],
    'Mean Bunch Energy': [0., 50.],
    'energy spread of the beam': [1., 1000.],
    'Correlation xpx': [-1., 1.],
}

for i, ax in enumerate(axes.flatten()):
    if i >= len(to_plot):
        ax.remove()
        continue
    col = to_plot[i]
    ax.plot(s, y[col])
    ax.fill_between(x=uncertainties['Path length'],
                    y1=y[col] - uncertainties[col],
                    y2=y[col] + uncertainties[col],
                    alpha=0.3)
    ax.set_ylim(limits[col])
    ax.set_xlabel('Path length [m]', fontsize=40)
    ax.set_ylabel(labels[col], fontsize=40)
    ax.tick_params(labelsize=20)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

axes[0, 1].set_yscale('log')

fig.align_labels()
fig.tight_layout()

st.pyplot(fig)
