import numpy as np
import joblib
from mllib.model import Preprocessor, StandardScaler


class ScatMat_SpecificScalers(Preprocessor):
    '''
    Preprocessor performing applying different scalers on columns containing P11 than PPF.
    Attention: Assumption that P11 is the first half of the dataset while PPF is the second!
    Input:
        Scaler_P11: Scaler to be applied to 'P11'
        Scaler_PPF: Scaler to be applied to 'PPF'
        P_11_column_loc: Indices of columns containing P11 scattering data
        P_12_column_loc: Indices of columns containing PPF (P12) scattering data
        LogP11: (Bool) True if logarithm is to be applied to P11 (which is greater equal 0)
        angles: Number of measurement angles
    '''
    def __init__(self, Scaler_P11=StandardScaler(), Scaler_PPF=StandardScaler(),
                 P_11_column_loc=[], P_12_column_loc=[], LogP11=False, angles=160):
        self.Scaler_P11 = Scaler_P11
        self.Scaler_PPF = Scaler_PPF
        self.P_11_column_loc = P_11_column_loc
        self.P_12_column_loc = P_12_column_loc
        self.LogP11 = LogP11
        self.angles = angles

    def fit(self, X):
        # Find P11 and PPF columns
        wavelengths = int(X.shape[1]/(2*self.angles))
        # We will assume P11 are first, then PPF
        self.P_11_column_loc = np.arange(0, self.angles*wavelengths)
        self.P_12_column_loc = np.arange(self.angles*wavelengths, 2*self.angles*wavelengths)

        # Apply logarithm to P11 and if so, copy dataset
        if self.LogP11:
            if not np.all((X[:, self.P_11_column_loc]) >= 0):
                print("Some elements of P11 are negative. Logarithm will cause trouble.")
            Z = X.copy()
            Z[:, self.P_11_column_loc] = np.log(Z[:, self.P_11_column_loc])
        else:
            Z = X
        # Fit scalers
        self.Scaler_P11.fit(Z[:, self.P_11_column_loc])
        self.Scaler_PPF.fit(Z[:, self.P_12_column_loc])
        return self

    def transform(self, X, y=None):
        Z = X.copy()
        # Apply logarithm to P11
        if self.LogP11:
            Z[:, self.P_11_column_loc] = np.log(Z[:, self.P_11_column_loc])
        # Apply scalers
        Z[:, self.P_11_column_loc] = self.Scaler_P11.transform(
            Z[:, self.P_11_column_loc])
        Z[:, self.P_12_column_loc] = self.Scaler_PPF.transform(
            Z[:, self.P_12_column_loc])
        return Z

    def fit_transform(self, X, y=None):
        self = self.fit(X)
        return self.transform(X, y)

    def inverse_transform(self, X, y=None):
        Z = X.copy()
        if self.LogP11:
            # Apply exponential
            Z[:, self.P_11_column_loc] = np.exp(
                self.Scaler_P11.inverse_transform(Z[:, self.P_11_column_loc]))
        else:
            Z[:, self.P_11_column_loc] = self.Scaler_P11.inverse_transform(
                X[:, self.P_11_column_loc])
        Z[:, self.P_12_column_loc] = self.Scaler_PPF.inverse_transform(
            X[:, self.P_12_column_loc])
        return Z

    def save(self, filename):
        joblib.dump(self, filename)

    def get_version(self):
        '''Nothing to do: a stateless object'''
        return '1.0'

    @classmethod
    def load(cls, filename):
        return joblib.load(filename)

    @classmethod
    def get_fully_qualified_classname(cls):
        # Don't know if necessary
        return 'mllib.model.P11_PPF_SpecificScalers'
