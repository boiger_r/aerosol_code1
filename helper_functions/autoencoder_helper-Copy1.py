#!/usr/bin/env python
# coding: utf-8
# %%

# %%


import os
import tensorflow as tf
import pandas as pd
import numpy as np
tf.keras.backend.set_floatx('float64')
from tensorflow import keras
from tensorflow.keras import layers
import sklearn
from sklearn.model_selection import train_test_split, KFold
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.models import Model
from ray import tune
from helper_functions.scan_helper_functions import load_dataset_wo_split, calculate_metrics
from mllib.model import KerasSurrogate, AdaptiveMinMaxScaler, DummyPreprocessor, LogarithmTransform


# %%
class Autoencoder_New(Model):
    def __init__(self,qoi_dim, dvar_dim, alpha, n_encode_layers, n_decode_layers):
        super(Autoencoder_New,self).__init__()
        self.qoi_dim = qoi_dim
        self.dvar_dim = dvar_dim
        self.alpha = alpha
        self.n_encode_layers = n_encode_layers
        self.n_decode_layers = n_decode_layers
        
        self.scale_dvar = 100
        self.scale_qoi = 10
        
        layers = [tf.keras.layers.Input(shape=dvar_dim[1])]
        for size in range(n_encode_layers):
            layers.append(tf.keras.layers.Dense(qoi_dim[1], activation='sigmoid'))
        layers.append(tf.keras.layers.Dense(qoi_dim[1]))
        self.encoder = tf.keras.models.Sequential(layers)
        
        layers = [tf.keras.layers.Input(shape=qoi_dim[1])]
        for size in range(n_decode_layers):
            layers.append(tf.keras.layers.Dense(qoi_dim[1], activation='sigmoid'))
        layers.append(tf.keras.layers.Dense(dvar_dim[1]))
        self.decoder = tf.keras.models.Sequential(layers)
         
        

    def call(self,x):
        dvar = x[0]-self.scale_dvar
        print(dvar)
        qoi = x[1]
        encoded = self.encoder(dvar)
        decoded = self.decoder(encoded)
        dvar_diff = tf.math.add(dvar,-decoded)
        dvar_loss = tf.math.reduce_sum(tf.math.square(dvar_diff))/dvar.shape[1]       
        qoi_diff = tf.math.add(qoi,-encoded)
        qoi_loss = tf.math.reduce_sum(tf.math.square(qoi_diff))/qoi.shape[1]
        sum_loss = tf.math.add(dvar_loss, self.alpha *qoi_loss)
        self.add_loss(sum_loss )
        return decoded


# %%
def build_autoencoder_new(config):
    

# %%


def train_autoencoder_new(config):
    datafile = config['datafile']
    

    dvar_train, dvar_val, qoi_train, qoi_val = load_dataset_wo_split(datafile)
    
    
#    dvar_train, dvar_val, dvar_test, qoi_train, qoi_val, qoi_test = load_dataset(datafile)
    #dvar_train.T[2] = dvar_train.T[2]*1.e+08
    #dvar_val.T[2] = dvar_val.T[2]*1.e+08
#    dvar_test = np.array(dvar_test)
#    dvar_test.T[2] =dvar_test.T[2]*1.e+08    
    
    #Preprocess data
    min_max_scaler = MinMaxScaler(feature_range=(0, 1))
    dvar_train = min_max_scaler.fit_transform(dvar_train)
    dvar_val = min_max_scaler.fit_transform(dvar_val)
#    dvar_test = min_max_scaler.fit_transform(dvar_test)
    qoi_train = min_max_scaler.fit_transform(qoi_train)
    qoi_val = min_max_scaler.fit_transform(qoi_val)
#    qoi_test = min_max_scaler.fit_transform(qoi_test)
    
    
    alpha = config['alpha']
    learning_rate = config['learning_rate']
    n_encode_layers = config['n_encode_layers']
    n_decode_layers = config['n_decode_layers']
    epochs = config['epochs']
    batch_size = config['batch_size']
    
    train_data = [dvar_train,qoi_train]
    val_data = [dvar_val,qoi_val]
#    test_data = [dvar_test,qoi_test]
    
    autoencoder = Autoencoder_new(qoi_train.shape ,dvar_train.shape, alpha, n_encode_layers, n_decode_layers)    
    autoencoder.compile(optimizer = tf.keras.optimizers.Adam(learning_rate=learning_rate))
    # Train

    history = autoencoder.fit(train_data, train_data, 
                epochs = epochs,
                batch_size = batch_size,
                validation_data = (val_data,val_data))
    # save the model


    autoencoder.save(tune.get_trial_dir())
    
    hist_csv_file = tune.get_trial_dir()+'history_loss.csv'
    history_df = pd.DataFrame(history.history)
    history_df.to_csv(hist_csv_file,index = False)
    
    dvar_pred_train1 = autoencoder.predict(train_data)
    dvar_pred_val1 = autoencoder.predict(val_data)

    dvar_pred_train2 = autoencoder.decoder(qoi_train)
    dvar_pred_val2 = autoencoder.decoder(qoi_val)

    qoi_pred_train1 = autoencoder.encoder(dvar_train)
    qoi_pred_val1 = autoencoder.encoder(dvar_val)

    metrics_train_dvar1 = calculate_metrics(dvar_train, dvar_pred_train1,n_in = dvar_train.shape[1])
    metrics_train_dvar2 = calculate_metrics(dvar_train, dvar_pred_train2,n_in = dvar_train.shape[1])
    metrics_train_qoi1 = calculate_metrics(qoi_train, qoi_pred_train1,n_in = qoi_train.shape[1])

    metrics_val_dvar1 = calculate_metrics(dvar_val, dvar_pred_val1,n_in = dvar_train.shape[1])
    metrics_val_dvar2 = calculate_metrics(dvar_val, dvar_pred_val2,n_in = dvar_train.shape[1])
    metrics_val_qoi1 = calculate_metrics(qoi_val, qoi_pred_val1,n_in = qoi_train.shape[1])

    metrics = {}

    for key in metrics_train_dvar1.keys():
        metrics[f'{key}_train_dvar1'] = metrics_train_dvar1[key]
        metrics[f'{key}_train_dvar2'] = metrics_train_dvar2[key]
        metrics[f'{key}_train_qoi1'] = metrics_train_qoi1[key]
        metrics[f'{key}_val_dvar1'] = metrics_val_dvar1[key]
        metrics[f'{key}_val_dvar2'] = metrics_val_dvar2[key]
        metrics[f'{key}_val_qoi1'] = metrics_val_qoi1[key]
    metrics   
    #return autoencoder, metrics_train_dvar1, metrics_train_dvar2, metrics_train_qoi1,metrics_val_dvar1, metrics_val_dvar2, metrics_val_qoi1
    tune.report(**metrics)


# %%





# %%




