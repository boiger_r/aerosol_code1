#!/usr/bin/env python
# coding: utf-8
import os
import argparse
from datetime import datetime
from pathlib import Path
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split


# Functions helping to select the right files
def find_path_to_folder(foldername='aerosol_data'):
    """ Return path of the data folder.
    Supposing that the folder /aerosol_data/ containing the data is not further than
    3 levels above the current working directory
    """
    for level in range(0, 4):
        parent = Path(os.path.abspath('')).parents[level]
        if foldername in os.listdir(parent):
            return str(parent) + "/" + foldername
    path = input("Enter the path of the folder where the data is stored:")
    return path


def list_data(dir_path, suffix):
    """ Reurn list of filenames at path 'dir_path' with the given suffix
    """
    # [f for f in os.listdir(dir_path) if f.endswith('.' + suffix)]
    return [f for f in os.listdir(dir_path) if ('.' + suffix) in f]


def auto_match_file_names(data_dir, data_names, file_type="pkl"):
    """
    Save name of the files, if uniqely identifiable by containing P11 and P12, into data_names.
    Input:
            data_dir : str path to directory
            file_type : str file typem eg. "pkl"
            data_names : list ['name_phase_file', 'name_polar_file']
    Return:
            True if auto matching was successfull
            False if not
    """
    potential_data = list_data(data_dir, file_type)
    only_one_P11 = True
    only_one_P12 = True
    for element in potential_data:
        if "P12" in element and only_one_P12:
            # Test first for P12, in case P11 is contained twice (e.g. P12/P11)
            data_names[1] = element
            only_one_P12 = False
        elif "P11" in element and only_one_P11:
            data_names[0] = element
            only_one_P11 = False
        else:
            print("No automatic matching possible")
            return False
    if not (only_one_P11 or only_one_P12):
        print("\nSucessfully auto matched the following data sets:")
        print("P11:", data_names[0])
        print("P12:", data_names[1])
    return True


def identify_files(data_dir, suffix="pkl"):
    """ Identify the files to preprocess.
    Input:
            data_dir : str. path to folder
            suffix   : str, filetype 'hdf' or 'pkl'
    """
    data_names = ["", ""]
    # Further file types could be added to the following list to be supported
    pd_commands = {"hdf": "pd.read_hdf(", "pkl": "pd.read_pickle("}
    pd_command = pd_commands[suffix]

    # Show files contained in the chosen data set
    potential_data = list_data(data_dir, suffix)
    print("\nContent of the dataset folder with suffix \'" + suffix + "\':")
    print(potential_data)

    # try to automatically match filenames
    if auto_match_file_names(data_dir, data_names, suffix):
        data_phase = eval(pd_command + f'"{data_dir}{data_names[0]}"' + ")")  # P_11
        data_polar = eval(pd_command + f'"{data_dir}{data_names[1]}"' + ")")  # P_12
    else:
        data_names[0] = input(
            "Enter the name of the file corresponding to the phase data P11:")
        data_names[1] = input(
            "Enter the name of the file corresponding to polarization data P12:")
        assert data_names[0] in potential_data, "One file not contained in the selected folder."
        assert data_names[1] in potential_data, "One file not contained in the selected folder."
        data_phase = eval(pd_command + f'"{data_dir}{data_names[0]}"' + ")")
        data_polar = eval(pd_command + f'"{data_dir}{data_names[1]}"' + ")")
        # data_phase = pd.read_pickle(f'{data_dir}/{data_names[0]}')
        # data_polar = pd.read_pickle(f'{data_dir}/{data_names[1]}')
    return data_phase, data_polar


def identify_files_train_test(data_dir, suffix="hdf"):
    """ Find training and test set in a folder.
    Input:
            data_dir : str. path to folder
            suffix   : str, filetype 'hdf' or 'pkl'
    Output:
            train_data : identified training data
            test_data  : identified test data
    """
    data_names = ["", ""]
    # Further file types could be added to the following list to be supported
    pd_commands = {"hdf": "pd.read_hdf(", "pkl": "pd.read_pickle("}
    pd_command = pd_commands[suffix]

    potential_data = list_data(data_dir, suffix)
    for element in potential_data:
        if "train" in element:
            data_names[0] = element
        elif "test" in element:
            data_names[1] = element
    if not all(data_names):
        # One of training or test file was not found
        print("No automatic matching possible")
        return 0, 0
    print("Matched the following data sets:")
    print("Training data:", data_names[0])
    print("Test data:", data_names[1])
    print("Command executed:")
    print(pd_command + f'"{data_dir}{data_names[0]}"' + ", key='dvar')")

    train_data_obs = eval(pd_command + f'"{data_dir}{data_names[0]}"' + ", key='qoi')")
    train_data_stat = eval(pd_command + f'"{data_dir}{data_names[0]}"' + ", key='dvar')")
    test_data_obs = eval(pd_command + f'"{data_dir}{data_names[1]}"' + ", key='qoi')")
    test_data_stat = eval(pd_command + f'"{data_dir}{data_names[1]}"' + ", key='dvar')")
    return train_data_obs, train_data_stat, test_data_obs, test_data_stat


def no_path_find_raw_data(file_type="pkl", dat_set_choice=0):
    """ Find files of type file_type in a sub folder of aerosol_data.
    Input:
            data_dir        : str. path to folder
            file_type       : str, filetype 'hdf' or 'pkl'
            dat_set_choice  : int, the chosen subfolder
    Output:
            data_phase  : pandas frame containing the phase
            data_polar  : pandas frame containing the polarization
    """
    data_dir = find_path_to_folder()
    print("Available Data sets:")
    data_set_list = [name for name in os.listdir(data_dir)
                     if os.path.isdir(os.path.join(data_dir, name))]
    print(dict(enumerate(data_set_list)))
    print(str(data_set_list[int(dat_set_choice)]) + " was chosen.")
    data_dir = data_dir + "/" + data_set_list[int(dat_set_choice)] + "/"
    data_phase, data_polar = identify_files(data_dir, file_type)
    return data_phase, data_polar


def no_path_find_pre(file_type="hdf", dat_set_choice=0, pre_choice=0):
    """ Find files of type file_type in a sub folder of a subfolder of aerosol_data.
    Input:
            data_dir        : str. path to folder
            file_type       : str, filetype 'hdf' or 'pkl'
            dat_set_choice  : int, the chosen subfolder
            dat_set_choice  : int, the chosen preprocessing folder
    Output:
            data_phase  : pandas frame containing the phase
            data_polar  : pandas frame containing the polarization
    """
    data_dir = find_path_to_folder()
    print("Available Data sets:")
    data_set_list = [name for name in os.listdir(data_dir)
                     if os.path.isdir(os.path.join(data_dir, name))]
    print(dict(enumerate(data_set_list)))
    print(str(data_set_list[int(dat_set_choice)]) + " was chosen.")
    data_dir = data_dir + "/" + data_set_list[int(dat_set_choice)] + "/"
    # Go to any sub-folder
    print("From the preprocessings:")
    data_set_list = [name for name in os.listdir(data_dir)
                     if os.path.isdir(os.path.join(data_dir, name))
                     and name.startswith("pre")]
    print(dict(enumerate(data_set_list)))
    print(str(data_set_list[int(dat_set_choice)]) + " was chosen.")
    data_dir = data_dir + "/" + data_set_list[int(dat_set_choice)] + "/"
    return identify_files_train_test(data_dir, file_type)


# Functions for preprocessing the data
def seperate_stateVar_from_obsVar(dataFrame):
    """ Separate the state and observation variables.
    Assuming that only state variables are labeled with strings,
    since all angles should be labeled with floats or integers.
    Input:
            dataFrame - panda data frame containing both state observation variables
    Output:
            state_var - panda data frame containing state variables
            obs_var - panda data frame containing observation variables
    """
    # Find all column labeled with strings
    str_labeled_columns = [isinstance(label, str)
                           for label in list(dataFrame.columns)]
    state_var = dataFrame.columns[str_labeled_columns]
    # observable are the complement of the above.
    obs_var = dataFrame.columns[np.invert(str_labeled_columns)]
    assert(len(obs_var) <= 180)
    return dataFrame[state_var], dataFrame[obs_var]


def remove_columns(df, column_names):
    """ Remove degrees which we cannot measure.
    These are [0:5], [75:85], [175:180].
    """
    bad_degrees = [*range(0, 5)] + [*range(75, 86)] + [*range(175, 180)]
    drop_columns = [f'{p}_{degree}.0' for degree in bad_degrees for p in column_names]
    df.drop(drop_columns, axis=1, inplace=True)
    pass


def seperate_and_save_test_data(dataFrame_stat, dataFrame_obs, dir_set_pre,
                                test_size=0.2, random_state=42):
    """ Randomly partition the dataFrame into a training and a test set and save to file.
    Input:
            dataFrame_stat : panda data frame containing state variables
            dataFrame_obs  : panda data frame containing observation variables
            dir_set_pre    : str path to the directory, in which the preprocessed data is to be
                             saved
            test_size      : float ratio of the test set
            random_state   : int seed
    """
    X_train, X_test, y_train, y_test = train_test_split(dataFrame_stat, dataFrame_obs,
                                                        test_size=test_size,
                                                        random_state=random_state)
    # By changing the next line, one could seperate the training data into a different folder
    dir_train_set = dir_set_pre
    dir_test_set = dir_set_pre
    # Save the training set
    X_train.to_hdf(dir_train_set + 'data_train' + '.hdf5', key='dvar')
    y_train.to_hdf(dir_train_set + 'data_train' + '.hdf5', key='qoi')
    # Save the test set
    X_test.to_hdf(dir_test_set + 'data_test' + '.hdf5', key='dvar')
    y_test.to_hdf(dir_test_set + 'data_test' + '.hdf5', key='qoi')
    pass


def add_relative_gaussian_noise(dataFrame, relative_error=0.05, seed=42):
    """ Add gaussian noise with a given relative error to dataFrame
        according to the following formula:
       [1 + rel_error * gauss(mean=0, std=0.5)]*dataFrame.
    Input:
            dataFrame       : pandas DataFrame containing the simulated observed data
            relative_error  : float between 0 and 1
            seed            : int seed for numpy's random generator
    Output:
            dataFrame_np_w_noise :  pandas DataFrame containing the data with
                                    noise added
    We assume that the relative error is associated to a confidence intervall with a
    p-value of p = 0.9545. Consequently, the standarddeviation is set to 0.5
    so that statistically about 95% of N samples are found in the interval [0,1].
    """
    np.random.seed(seed)
    dataFrame_np = dataFrame.to_numpy()
    # If one wishes to change the p-value, the standard deviation in the next line must be changed
    noise = relative_error * np.random.normal(0, 0.5, dataFrame_np.shape)
    dataFrame_np_w_noise = pd.DataFrame((1 + noise) * dataFrame_np,
                                        columns=dataFrame.keys())
    return dataFrame_np_w_noise


def add_absolute_gaussain_noise(dataFrame, absolute_error=0.08, seed=42):
    """ Add gaussian noise with a given absolute error to dataFrame
        according to the following formula:
        dataFrame + absolute_error * gauss(mean=0, std=0.5)
    Input:
            dataFrame       : pandas DataFrame containing the simulated observed data
            absolute_error  : float between 0 and 1
            seed            : int seed for numpy's random generator
    Output:
            dataFrame_np_w_noise :  pandas DataFrame containing the data with
                                    noise added
    We assume that the relative error is associated to a confidence intervall with a
    p-value of p = 0.9545. Consequently, the standarddeviation is set to 0.5
    so that statistically about 95% of N samples are found in the interval [0,1].
    """
    np.random.seed(seed)
    dataFrame_np = dataFrame.to_numpy()
    # If one wishes to change the p-value, the standard deviation in the next line must be changed
    noise = absolute_error * np.random.normal(0, 0.8, dataFrame_np.shape)
    dataFrame_np_w_noise = pd.DataFrame(dataFrame_np + noise, columns=dataFrame.keys())
    return dataFrame_np_w_noise


def str2bool(v):
    """ Facilitate the us of boolean variables in argparse.
    """
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == '__main__':
    # Parse the command line arguments.
    parser = argparse.ArgumentParser()
    parser.add_argument('--dir', type=str, default='none',
                        help='Path to the folder containing the .pkl files with the datasets.')
    parser.add_argument('--noise', default=True, type=str2bool,
                        help='1 to add noise to data, 0 else.')
    parser.add_argument('--rel_error', default=0.05, type=float,
                        help='Relative error between 0 and 1. noise==1 to have an effect')
    parser.add_argument('--abs_error', default=0.08, type=float,
                        help='Absolute error. noise==1 to have an effect')
    parser.add_argument('--log', default=False, type=str2bool,
                        help='1 to apply the log to the phase part of the observation, 0 else')
    parser.add_argument('--test_size', default=0.2, type=float,
                        help='Test size between 0 and 1')
    parser.add_argument('--seed', default=42, type=int,
                        help='Random seed')
    parser.add_argument('--keep_polar', default=False, type=str2bool,
                        help='Keep the phase_data. Default false')
    parser.add_argument('--file_type', default="pkl", type=str,
                        choices=["hdf", "pkl"],
                        help='File type of the input data')

    # Check if arguments are within bounds
    args = parser.parse_args()
    data_dir = args.dir
    if (args.rel_error < 0) or (args.rel_error > 1):
        argparse.ArgumentTypeError("Relative error must be between 0 and 1")
        quit()
    if (args.test_size < 0) or (args.test_size > 1):
        argparse.test_size("Test size must be between 0 and 1")
        quit()

    # If no path was handed in
    if data_dir == 'none':
        data_dir = find_path_to_folder()
        print("Available Data sets:")
        data_set_list = [name for name in os.listdir(data_dir)
                         if os.path.isdir(os.path.join(data_dir, name))]
        print(dict(enumerate(data_set_list)))
        repeat = True
        while repeat:
            choice = input(
                "Choose a data set from the list above by entering its ID: ")
            repeat = True
            if choice.isnumeric():
                repeat = False
                if not (0 <= int(choice) <= len(data_set_list)-1):
                    repeat = True
            if repeat:
                print("Error: Id out of range " + f"[0, {len(data_set_list)-1}]. Try again.")
        data_dir = data_dir + "/" + data_set_list[int(choice)] + "/"

    # Identify the files to prprocess
    data_phase, data_polar = identify_files(data_dir, args.file_type)

    # Seperate state variables from observational variables
    data_polar_state, data_polar_observation = seperate_stateVar_from_obsVar(
        data_polar)
    data_phase_state, data_phase_observation = seperate_stateVar_from_obsVar(
        data_phase)

    # Add noise
    if args.noise:
        data_phase_observation = add_relative_gaussian_noise(
            data_phase_observation, args.abs_error)
        data_polar_observation = add_absolute_gaussain_noise(
            data_polar_observation, args.rel_error)

    # Take logarithm of phase part
    if args.log:
        data_phase_observation = np.log(data_phase_observation)

    # Merge data_polar_state and data_phase_state
    assert all(data_polar_state ==
               data_phase_state), "Merging phase and polar frames impossible"
    data_state = data_polar_state
    data_state.name = 'data_state'
    data_polar_observation.columns = ['polar_' + str(key) for key
                                      in data_polar_observation.columns]
    data_phase_observation.columns = ['phase_' + str(key) for key
                                      in data_phase_observation.columns]
    # Join data_polar_state and data_phase_state
    if args.keep_polar:
        data_all_observation = pd.concat(
            [data_polar_observation, data_phase_observation], axis=1)
        remove_columns(data_all_observation, ["polar", "phase"])
    # Keep only the polar part of the observation
    else:
        data_all_observation = data_polar_observation
        remove_columns(data_all_observation, ["phase"])
    data_all_observation.name = 'data_all_observation'

    # Prepear for ML
    # Create folder to save the preprocessed data to
    process_specification = (['', 'relErr-' + str(args.rel_error) + '_'][args.noise]
                             + ['', 'log_'][args.log] + 'ts-' + str(args.test_size)
                             + ['', '_p_11'][not args.keep_polar])
    dir_set_pre = data_dir + 'pre_' + process_specification
    if os.path.exists(dir_set_pre):
        dir_set_pre += datetime.now().strftime('_%H_%S') + '/'
        os.makedirs(dir_set_pre)
    else:
        dir_set_pre += '/'
        os.makedirs(dir_set_pre)

    # Seperate test data
    seperate_and_save_test_data(data_state, data_all_observation, dir_set_pre,
                                test_size=args.test_size, random_state=args.seed)

    # Create log message
    log_msg = ("This preprocessed partition of the data set \'"
               + data_set_list[int(choice)]
               + "\' \nwas produced using "
               "\'\helper_functions\preprocessing.py\'. \n \n"
               "The data was subjected to the following procedure: \n")
    pro_msg = ""
    if args.keep_polar:
        if args.noise:
            pro_msg += ("- Add 5% relative gaussain noise to P_11. \n"
                        "- Add 8% absolute gaussain noise to P_12. \n")
        if args.log:
            pro_msg += ("- Take log of P_11 \n"
                        "- Take log of P_12 \n")
        pro_msg += ("- Randomly partition data into " + (str(1 - args.test_size)
                    + "% training and ") + str(args.test_size) + "% test set.\n")
        pro_msg += ("\nSeed used: " + str(args.seed))
    else:
        pro_msg += ("- Only process P_11. \n")
        if args.noise:
            pro_msg += ("- Add 5% relative gaussain noise to P_11. \n")
        if args.log:
            pro_msg += ("- Take log of P_11 \n")
        pro_msg += ("- Randomly partition data into " + (str(1 - args.test_size)
                    + "% training and ") + str(args.test_size) + "% test set.\n")
        pro_msg += ("\nSeed used: " + str(args.seed))
    print("".center(80, '#'))
    print('\33[1m' + "Data set successfully processed, partitioned and saved to" + '\33[0m')
    print(dir_set_pre)
    print("\nLog:")
    # Create log file
    print(pro_msg)
    log_msg += pro_msg
    print("".center(80, '#') + "\n")
    with open(dir_set_pre + "log.txt", "w") as log_file:
        print(log_msg, file=log_file)
