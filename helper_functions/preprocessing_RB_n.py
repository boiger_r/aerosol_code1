#!/usr/bin/env python
# coding: utf-8
import os
import argparse
from datetime import datetime
from pathlib import Path
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, MinMaxScaler
#from sklearn.externals.joblib import dump
from pickle import dump


# Functions helping to select the right files
def find_path_to_folder(foldername='aerosol_data'):
    """ Return path of the data folder.
    Supposing that the folder /aerosol_data/ containing the data is not further than
    3 levels above the current working directory
    """
    for level in range(0, 4):
        parent = Path(os.path.abspath('')).parents[level]
        if foldername in os.listdir(parent):
            return str(parent) + "/" + foldername
    path = input("Enter the path of the folder where the data is stored:")
    return path


def list_data(dir_path, suffix):
    """ Reurn list of filenames at path 'dir_path' with the given suffix
    """
    # [f for f in os.listdir(dir_path) if f.endswith('.' + suffix)]
    return [f for f in os.listdir(dir_path) if ('.' + suffix) in f]


def auto_match_file_names(data_dir, data_names, file_type="csv"):
    """
    Save name of the files, if uniqely identifiable by containing P11 and P12, into data_names.
    Input:
            data_dir : str path to directory
            file_type : str file typem eg. "csv"
            data_names : list ['name_obs_file', 'name_param_file']
    Return:
            True if auto matching was successfull
            False if not
    """
    potential_data = list_data(data_dir, file_type)
    only_one_param = True
    only_one_obs = True
    for element in potential_data:
        if "scat" in element and only_one_obs:
            data_names[0] = element
            only_one_obs = False
        elif "par" in element and only_one_param:
            data_names[1] = element
            only_one_param = False
        else:
            print("No automatic matching possible")
            return False
    if not (only_one_param or only_one_obs):
        print("\nSucessfully auto matched the following data sets:")
        print("Observables:", data_names[0])
        print("Parameters:", data_names[1])
    return True


def identify_raw_files(data_dir, suffix="csv"):
    """ Identify the files to preprocess.
    Input:
            data_dir : str. path to folder
            suffix   : str, filetype 'hdf' or 'pkl'
    """
    data_names = ["", ""]
    # Further file types could be added to the following list to be supported
    pd_commands = {"hdf": "pd.read_hdf(", "pkl": "pd.read_pickle(", "csv": "pd.read_csv("}
    pd_command = pd_commands[suffix]

    # Show files contained in the chosen data set
    potential_data = list_data(data_dir, suffix)
    print("\nContent of the dataset folder with suffix \'" + suffix + "\':")
    print(potential_data)

    # See if list is empty
    if not potential_data:
        return False, False

    # try to automatically match filenames
    if auto_match_file_names(data_dir, data_names, suffix):
        data_obs = eval(pd_command + f'"{data_dir}{data_names[0]}"' + ")")  # Observational
        data_param = eval(pd_command + f'"{data_dir}{data_names[1]}"' + ")")  # State
        return data_obs, data_param
    else:
        return False, False


def identify_preprocessed_files(data_dir, suffix="hdf"):
    """ Find training and test set in a folder.
    Input:
            data_dir : str. path to folder
            suffix   : str, filetype 'hdf' or 'pkl'
    Output:
            train_data : identified training data
            test_data  : identified test data
    """
    data_names = ["", ""]
    # Further file types could be added to the following list to be supported
    pd_commands = {"hdf": "pd.read_hdf(", "pkl": "pd.read_pickle("}
    pd_command = pd_commands[suffix]

    potential_data = list_data(data_dir, suffix)
    for element in potential_data:
        if "train" in element:
            data_names[0] = element
        elif "test" in element:
            data_names[1] = element
    if not all(data_names):
        # One of training or test file was not found
        print("No automatic matching possible")
        return 0, 0
    print("Matched the following data sets:")
    print("Training data:", data_names[0])
    print("Test data:", data_names[1])
    print("Command executed:")
    print(pd_command + f'"{data_dir[:-1]}{data_names[0]}"' + ", key='dvar')")

    train_data_obs = eval(pd_command + f'"{data_dir}{data_names[0]}"' + ", key='qoi')")
    train_data_stat = eval(pd_command + f'"{data_dir}{data_names[0]}"' + ", key='dvar')")
    test_data_obs = eval(pd_command + f'"{data_dir}{data_names[1]}"' + ", key='qoi')")
    test_data_stat = eval(pd_command + f'"{data_dir}{data_names[1]}"' + ", key='dvar')")
    return train_data_obs, train_data_stat, test_data_obs, test_data_stat


def no_path_find_pre(file_type="hdf", dat_set_choice=1, pre_choice=0):
    """ Find files of type file_type in a sub folder of a subfolder of aerosol_data.
    Input:
            data_dir        : str. path to folder
            file_type       : str, filetype 'hdf' or 'pkl'
            dat_set_choice  : int, the chosen subfolder
            dat_set_choice  : int, the chosen preprocessing folder
    Output:
            data_phase  : pandas frame containing the phase
            data_polar  : pandas frame containing the polarization
    """
    data_dir = find_path_to_folder()
    print("Available Data sets:")
    data_set_list = [name for name in os.listdir(data_dir)
                     if os.path.isdir(os.path.join(data_dir, name))]
    print(dict(enumerate(data_set_list)))
    print(str(data_set_list[int(dat_set_choice)]) + " was chosen.")
    data_dir = data_dir + "/" + data_set_list[int(dat_set_choice)] + "/"
    # Go to any sub-folder
    print("From the preprocessings:")
    data_set_list = [name for name in os.listdir(data_dir)
                     if os.path.isdir(os.path.join(data_dir, name))
                     and name.startswith("pre")]
    print(dict(enumerate(data_set_list)))
    print(str(data_set_list[int(pre_choice)]) + " was chosen.")
    data_dir = data_dir + "/" + data_set_list[int(pre_choice)] + "/"
    return identify_preprocessed_files(data_dir, file_type)


# Functions for preprocessing the data
def add_relative_gaussian_noise(dataFrame, relative_error=0.05, seed=42):
    """ Add gaussian noise with a given relative error to dataFrame
        according to the following formula:
       [(1 + rel_error) * gauss(mean=0, std=0.5)]*dataFrame.
    Input:
            dataFrame       : pandas DataFrame containing the simulated observed data
            relative_error  : float between 0 and 1
            seed            : int seed for numpy's random generator
    Output:
            dataFrame_np_w_noise :  pandas DataFrame containing the data with
                                    noise added
    We assume that the relative error is associated to a confidence intervall with a
    p-value of p = 0.9545. Consequently, the standarddeviation is set to 0.5
    so that statistically about 95% of N samples are found in the interval [0,1].
    """
    dataFrame_np = dataFrame.to_numpy()
    # If one wishes to change the p-value, the standard deviation in the next line must be changed
    noise = relative_error * np.random.normal(0, 0.5, dataFrame_np.shape)
    dataFrame_np_w_noise = pd.DataFrame((1 + noise) * dataFrame_np,
                                        columns=dataFrame.keys())
    return dataFrame_np_w_noise


def add_absolute_gaussain_noise(dataFrame, absolute_error=0.08, seed=42):
    """ THIS NOT YET DONE PROPERLY!
    Add gaussian noise with a given absolute error to dataFrame
        according to the following formula:
       [rel_error * gauss(mean=0, std=0.5)] + dataFrame.
    Input:
            dataFrame       : pandas DataFrame containing the simulated observed data
            relative_error  : float between 0 and 1
            seed            : int seed for numpy's random generator
    Output:
            dataFrame_np_w_noise :  pandas DataFrame containing the data with
                                    noise added
    We assume that the absolute error is associated to a confidence intervall with a
    p-value of p = 0.9545. Consequently, the standarddeviation is set to 0.5
    so that statistically about 95% of N samples are found in the interval [0,1].
    """
    dataFrame_np = dataFrame.to_numpy()
    # If one wishes to change the p-value, the standard deviation in the next line must be changed
    noise = absolute_error * np.random.normal(0, 0.5, dataFrame_np.shape)
    dataFrame_np_w_noise = pd.DataFrame(noise + dataFrame_np,
                                        columns=dataFrame.keys())
    return dataFrame_np_w_noise


def normalization(U):
    """ Perform normalization by average over all angels and samples.
    Inspired by Vladimir V. Berdnik, and Valery A. Loiko
    in 'Retrieval of size and refractive index of spherical particles by
    multiangle light scattering: neural network method application'.
    However without using a logarithm.
    Input:
            U                              : pandas DataFrame containing the scattering data
    Output:
            Y                              : Normalized data
            Normalization_factor_per_angle : Average over samples per angle
            Normalization_factor_per_sample: Average over angles per sample
    The two normalization factors are returend for potential inverse transformations.
    """
    # Y = U/(U_bar*np.average(U, axis=1)[:, None])  # correct
    Normalization_factor_per_angle = np.average(U, axis=0)
    Normalization_factor_per_sample = np.average(U, axis=1)
    Normalized_per_sample = U/Normalization_factor_per_sample[:, None]
    Y = Normalized_per_sample/Normalization_factor_per_angle
    return Y, Normalization_factor_per_angle, Normalization_factor_per_sample


def normalization_test(U, Normalized_per_angle):
    """ Perform normalization by average over all angels and samples using
    the average over samples per angle from the traing data.
    Input:
            U                    : pandas DataFrame containing the scattering data
            Normalized_per_angle : Average over samples per angle of training data
    Output:
            Y                    : Normalized data
    """
    # Y = U/(U_bar*np.average(U, axis=1)[:, None])  # correct
    Normalization_factor_per_sample = np.average(U, axis=1)
    Normalized_per_sample = U/Normalization_factor_per_sample[:, None]
    Y = Normalized_per_sample/Normalized_per_angle
    return Y, Normalization_factor_per_sample


def normalize(X_train, X_test, wavelens):
    """ Normalize data per wavelenth per element of scattering matrix
    with respect to angle and sample mean.
    Input:
            X_train         : panda data frame containing scattering data of the training dataset
            X_train         : panda data frame containing scattering data of the test dataset
            wavelens        : List of wavelengths considered
    Output:
            p_per_angle     : Average over samples per angle of the training dataset
            p_per_sample    : Average over angles per sample of the training dataset
            p_per_sample_t  : Average over angles per sample of the test dataset
    Terribly laborious!
    """
    # Initialize variables
    number_of_angles = len(list(X_train.filter(regex='(?=.*P11)(?=.*' + str(wavelens[0]) + ')')))
    number_of_samples = X_train.shape[0]
    number_of_samples_t = X_test.shape[0]
    if len(list(X_train.filter(regex='P12', axis=1))) != 0:  # P11 and P12
        elements_scat_matrix = ['P11', 'P12']
        wavelen_str = ["P11_" + str(_) for _ in wavelens] + ["P12_" + str(_) for _ in wavelens]
        p_per_angle = pd.DataFrame(np.zeros((number_of_angles, 2*len(wavelens))),
                                   columns=wavelen_str)
        p_per_sample = pd.DataFrame(np.zeros((number_of_samples, 2*len(wavelens))),
                                    columns=wavelen_str)  # for training dataset
        p_per_sample_t = pd.DataFrame(np.zeros((number_of_samples, 2*len(wavelens))),
                                      columns=wavelen_str)  # for test test dataset
    else:  # Only P11
        elements_scat_matrix = ['P11']
        wavelen_str = ["P11_" + str(item) for item in wavelens]
        p_per_angle = pd.DataFrame(np.zeros((number_of_angles, len(wavelens))),
                                   columns=wavelen_str)
        p_per_sample = pd.DataFrame(np.zeros((number_of_samples, len(wavelens))),
                                    columns=wavelen_str)  # for training dataset
        p_per_sample_t = pd.DataFrame(np.zeros((number_of_samples_t, len(wavelens))),
                                      columns=wavelen_str)  # for test test dataset
    X_train.is_copy = False
    X_test.is_copy = False
    p_per_angle.is_copy = False
    # Perform normalization
    for smat_ind, pmat in enumerate(elements_scat_matrix):
        for index, wl in enumerate(wavelens):
            # Identify the relevant columns, then find slice
            wl_index = list(X_train.filter(regex='(?=.*' + pmat + ')(?=.*' + str(wl) + ')'))
            # Maybe sort, just in case
            be, en = wl_index[0], wl_index[-1]
            # Perform normalization by average over all angels and samples
            X_train.loc[:, be:en], p_per_angle.iloc[:, (smat_ind + 1) * index], p_per_sample.iloc[:, (
                smat_ind + 1) * index] = normalization(X_train.loc[:,
                be:en].to_numpy())
            X_test.loc[:, be:en], p_per_sample_t.iloc[:, (smat_ind + 1) * index] = normalization_test(
                X_test.loc[:, be:en],
                p_per_angle.iloc[:, index].to_numpy())
    return p_per_angle, p_per_sample, p_per_sample_t


def apply_scaler(X_train, X_test, wavelengths, dir_train_set, scaler):
    """ Apply a scaler per wavelength per element of scattering matrix.
    Input:
            X_train         : panda data frame containing scattering data of the training dataset
            X_test          : panda data frame containing scattering data of the test dataset
            wavelens        : List of wavelengths considered
            dir_train_set   : path to where training dataset will be stored
            scaler          : Scikit scaler to be applied StandardScaler() or MinMaxScaler()
    """
    if scaler=='StandardScaler':
        scaler = StandardScaler()
    if scaler=='MinMaxScaler':
        scaler = MinMaxScaler()
    # Initialize variables
    if len(list(X_train.filter(regex='P12', axis=1))) != 0:  # P11 and P12
        elements_scat_matrix = ['P11', 'P12']
    else:  # Only P11
        elements_scat_matrix = ['P11']
    X_train.is_copy = False
    X_test.is_copy = False
    # Perform normalization
    for smat_ind, pmat in enumerate(elements_scat_matrix):
        for index, wl in enumerate(wavelengths):
            # Identify the relevant columns
            #wl_index = list(X_train.filter(regex='(?=.*' + pmat + ')(?=.*' + str(wl) + ')'))
            wl_index = list(X_train.filter(regex='(?=.*' + pmat + ')(?=.*' + wl + ')'))
            # Sort, just in case
            be, en = wl_index[0], wl_index[-1]
            # Apply scaler
            X_train.loc[:, be:en] = scaler.fit_transform(X_train.loc[:, be:en])
            X_test.loc[:, be:en] = scaler.transform(X_test.loc[:, be:en])
            # Save scaler to file for potential inverse transformation
            dump(scaler, open(dir_train_set + 'std_scaler' + pmat + wl
                              + '.pickle', 'wb'))
    pass


def seperate_and_save_test_data(dataFrame_stat, dataFrame_obs, dir_set_pre, scaler,normalize_data,  test_size, random_state ):
    """ Randomly partition the dataFrame into a training and a test set and save to file.
    Input:
            dataFrame_stat : panda data frame containing state variables
            dataFrame_obs  : panda data frame containing observation variables
            dir_set_pre    : str path to the directory, in which the preprocessed data is to be
                             saved
            test_size      : float ratio of the test set
            random_state   : int seed
    """
    X_train, X_test, y_train, y_test = train_test_split(dataFrame_stat, dataFrame_obs,
                                                        test_size=test_size,
                                                        random_state=random_state)
    # By changing the next line, one could seperate the training data into a different folder
    dir_train_set = dir_set_pre
    dir_test_set = dir_set_pre

    # Normalize data
    if normalize_data:
        p_per_angle, p_per_sample, p_per_sample_t = normalize(y_train, y_test, wavelengths)
        # Save training scalers
        p_per_angle.to_hdf(dir_train_set + 'training_dataset' + '.hdf5', key='scale_a')
        p_per_sample.to_hdf(dir_train_set + 'training_dataset' + '.hdf5', key='scale_s')
        # Save test scalers
        p_per_sample_t.to_hdf(dir_test_set + 'training_dataset' + '.hdf5', key='scale_s')

    # Apply a scaler, such as MinMaxScaler or StandardScaler or None
    if scaler == 'None':
        pass
    else:
        apply_scaler(y_train, y_test, wavelengths, dir_train_set, scaler)

    # Save the training set
    X_train.to_hdf(dir_train_set + 'training_dataset' + '.hdf5', key='dvar')
    y_train.to_hdf(dir_train_set + 'training_dataset' + '.hdf5', key='qoi')

    # Save the test set
    X_test.to_hdf(dir_test_set + 'test_dataset' + '.hdf5', key='dvar')
    y_test.to_hdf(dir_test_set + 'test_dataset' + '.hdf5', key='qoi')
    print('\n')
    print('dvar', X_test)  # aeorosol properties
    print('qoi', y_test)  # scattering data
    pass


def str2bool(v):
    """ Facilitate the use of boolean variables in argparse.
    """
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == '__main__':
    # Parse the command line arguments.
    parser = argparse.ArgumentParser()
    parser.add_argument('--dir', type=str, default='none',
                        help='Path to the folder containing the files with the datasets.')
    parser.add_argument('--log', default=False, type=str2bool,
                        help='1 to apply the log to the phase part of the observation, 0 else')
    parser.add_argument('--wavelength',  default="532", type=str,
                        help=('Wavelength to be used. Either any of'
                        ' ["450", "532", "630"] or "all". For example'
                        ' "450,630" is a legal combination.'))
    parser.add_argument('--test_size', default=0.2, type=float,
                        help='Test size between 0 and 1')
    parser.add_argument('--seed', default=42, type=int,
                        help='Random seed')
    parser.add_argument('--keep_polar', default=False, type=str2bool,
                        help='Keep the polarized phase_data. Default false')    
    parser.add_argument('--keep_phase', default=True, type=str2bool,
                        help='Keep the phase_data. Default false')
    parser.add_argument('--trunc_angle', nargs=4, type=int,
                        default=[5, 85, 96, 176],
                        help=('Truncation angles to be used. A sortet tuple'
                        ' of for integers. For example'
                        ' 5 85 96 176 is default and results in'
                        ' the intervals [0:5), [85:96) and [176:180)'
                        ' being left out.'))
    parser.add_argument('--file_type', default="csv", type=str,
                        choices=["hdf", "pkl", "csv"],
                        help='File type of the input data')
    parser.add_argument('--normalize', default=False, type=str2bool,
                        help='1 to apply normalization by average over'
                        ' angle and sample per wavelength and per element'
                        ' of the scattering matrix')
    parser.add_argument('--scaler', default="None", type=str,
                        choices=["MinMaxScaler", "StandardScaler","None"],
                        help='Scaler to be applied per wavelength per element'
                        ' of the scattering matrix')

    # Check if arguments are within bounds
    args = parser.parse_args()
    seed = args.seed
    np.random.seed(seed)
    
    trunc_ang = np.sort(args.trunc_angle)  # Make certain it is sorted
    assert (np.all(trunc_ang >= 0) and np.all(trunc_ang < 180)),"Truncation angles out of range"    


    data_dir = args.dir

    # Initialize wavelength
    if args.wavelength == 'all':
        wavelengths = ["450", "532", "630"]
    else:
        wavelengths = [item.strip() for item in args.wavelength.split(',')]
        wavelengths.sort()  # Sort from smallest to largest
    print('wavelengths',wavelengths)
    # If no path was handed in
    if data_dir == 'none':
        data_dir = find_path_to_folder()
        print("Available Data sets:")
        data_set_list = [name for name in os.listdir(data_dir)
                         if os.path.isdir(os.path.join(data_dir, name))]
        print(dict(enumerate(data_set_list)))
        repeat = True
        while repeat:
            choice = input(
                "Choose a data set from the list above by entering its ID: ")
            repeat = True
            if choice.isnumeric():
                repeat = False
                if not (0 <= int(choice) <= len(data_set_list)-1):
                    repeat = True
            if repeat:
                print("Error: Id out of range " + f"[0, {len(data_set_list)-1}]. Try again.")
        data_dir = data_dir + "/" + data_set_list[int(choice)] + "/"

    # Identify the files to preprocess
    data_obs, data_param = identify_raw_files(data_dir, args.file_type)

    # Identify P11 and P12 in observables, assuming colum name contains P11/P12
    P11 = data_obs.loc[:, list(data_obs.filter(regex='P11', axis=1))]
    P12 = data_obs.loc[:, list(data_obs.filter(regex='P12', axis=1))]

    # Remove angles that we cannot measure, assuming column name ends with the f'_{degree}'
    # By default the truncation angles are assumed to be a tuple [5, 85, 96, 176]
    # resulting in the angles [0:5), [85:96) and [176:180) being left out
    
    if np.sum(np.array(trunc_ang)) == 0:
        bad_degrees = [0]
    else:
    
        bad_degrees = ([*range(0, trunc_ang[0])] + [*range(trunc_ang[1],
                    trunc_ang[2])] + [*range(trunc_ang[3], 180)])
        bad_degrees_underscore = [f'_{i}' for i in bad_degrees]
        bad_columns = [col for col in data_obs for deg in bad_degrees_underscore if col.endswith(deg)]
        P11.drop([col for col in bad_columns if 'P11' in col], axis=1, inplace=True)
        P12.drop([col for col in bad_columns if 'P12' in col], axis=1, inplace=True)
    # Reduce to one wavelength
    if not args.wavelength == 'all':
        P11 = P11.loc[:, list(P11.filter(regex='|'.join(map(str, wavelengths)), axis=1))]
        P12 = P12.loc[:, list(P12.filter(regex='|'.join(map(str, wavelengths)), axis=1))]

    # Remove unused data for parameters:
    use_column_param = list(data_param.filter(regex='|'.join(map(str,
                            wavelengths)), axis=1))
    use_column_param.append("V_tot")
    use_column_param.append("R_median")
    use_column_param.append("GSD")
    use_column_param.append("n")
    data_param = data_param.loc[:, use_column_param]

    # Take logarithm of P11
    if args.log:
        P11 = np.log(P11)

    # Rejoin P11 and P12, if necessary
    if args.keep_phase:
        if args.keep_polar:
            data_all_observation = pd.concat(
                [P11, P12], axis=1)
        # Keep only P11
        else:
            data_all_observation = P11
    # Keep only P12
    else:
        data_all_observation = P12
    data_all_observation.name = 'data_all_observation'
    

    # Prepear for ML
    # Create folder to save the preprocessed data to
    process_specification = (['', 'log_'][args.log] + 'ts-' + str(args.test_size)
                             + ['', '_P11'][not args.keep_polar]
                             + ['', '_P12'][not args.keep_phase]
                             + ['_wl-' + '-'.join(map(str, wavelengths)), ''][args.wavelength == 'all']
                             + ['_sc-' + args.scaler, ''][args.scaler == 'None']
                             + ['', '_nm'][args.normalize]
                             + '_ta-' + '-'.join(map(str, trunc_ang))
                             )
    dir_set_pre = data_dir + 'pre_' + process_specification
    if os.path.exists(dir_set_pre):
        dir_set_pre += datetime.now().strftime('_%H_%S') + '/'
        os.makedirs(dir_set_pre)
    else:
        dir_set_pre += '/'
        os.makedirs(dir_set_pre)

    # Seperate test data
    seperate_and_save_test_data(data_param, data_all_observation, dir_set_pre,args.scaler, args.normalize, args.test_size, seed)



    # Create log message
    log_msg = ("This preprocessed partition of the data set \'"
               + data_set_list[int(choice)]
               + "\'\nwas produced using "
               "\'\helper_functions\preprocessing.py\'.\n\n"
               "The data was subjected to the following procedure:\n")
    pro_msg = ""
    
    if args.keep_phase:
        if not args.keep_polar:
            pro_msg += ("- Only process P_12.\n")
    else:
        if args.keep_polar:
            pro_msg += ("- Only process P_11.\n")
    
    if not args.wavelength == 'all':
        pro_msg += ("- Only process scattering data generated using the wavelength"
                    + ["s", ""][len(wavelengths) == 1] + " "
                    + ' nm, '.join(map(str, wavelengths[:-1]))
                    + [" nm and ", ""][len(wavelengths) == 1]
                    + wavelengths[-1] + " nm.\n")
        
        
    pro_msg += ("- Drop the angles at:\n" + "  " + ', '.join(map(str, bad_degrees[:-1]))
                + " and " + str(bad_degrees[-1])
                + " degrees.\n")
    pro_msg += ("- Randomly partition data into " + (str((1 - args.test_size)*100)
                + "% training and ") + str(args.test_size*100) + "% test set.\n")
    if args.log:
        pro_msg += ("- Take log of P_11\n")
    if not args.scaler=='None':
        pro_msg += ("- Fit_transform " + args.scaler + " on training data"
                                "and transform test data.\n")
    pro_msg += ("\nSeed used: " + str(seed))

        
        
    
    print("".center(80, '#'))
    print('\33[1m' + "Data set successfully processed, partitioned and saved to" + '\33[0m')
    print(dir_set_pre)
    print("\nLog:")
    # Create log file
    print(pro_msg)
    log_msg += pro_msg
    print("".center(80, '#') + "\n")
    with open(dir_set_pre + "log.txt", "w") as log_file:
        print(log_msg, file=log_file)
