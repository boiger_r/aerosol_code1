import os
import numpy as np
import pandas as pd
from ray import tune
from helper_functions.scan_helper_functions_configs import calculate_metrics
from sklearn.model_selection import train_test_split
import xgboost as xgb

def train_xgboost_model(config):  
    datafile = config['datafile']
    
    dvar = pd.read_hdf(datafile, key='dvar')
    qoi = pd.read_hdf(datafile, key='qoi')
    
    xtrain, xval, ytrain, yval = train_test_split(dvar, qoi, 
                                                  test_size = config['validation_split'],
                                                  random_state = None)    

    surr = build_xgboost_surrogate(config)
    surr.fit(xtrain,
             ytrain,
             eval_set=[(xtrain, ytrain), (xval, yval)],
             early_stopping_rounds=config['early_stopping_rounds'],
             eval_metric='rmse')
    surr.save_model(tune.get_trial_dir() + 'model.json')

    # Calculate the performance metrics
    n_in = xtrain.shape[1]    
    metrics_train = calculate_metrics(ytrain,
                                      surr.predict(xtrain).reshape(-1,1), n_in)
    metrics_val = calculate_metrics(yval,
                                    surr.predict(xval).reshape(-1,1), n_in)
    
    metrics = {}
    for key in metrics_train.keys():
        metrics[f'{key}_train'] = metrics_train[key]
        metrics[f'{key}_val'] = metrics_val[key]
        
    tune.report(**metrics)
        

def build_xgboost_surrogate(config):
    # build the surrogate model
    modelXGB = xgb.XGBRegressor(n_estimators = config["max_trees"],  # Maximum number of iterators. Can stop earlier if no improvement
                         max_depth = config["max_depth"],  # Recommended is 4 < J < 8
                         eta = config["learning_rate"],
                         subsample = config["subsample"],  # Don't use entire training set i.e. stochastic boosting
                         colsample_bytree = config["colsample"],  # Don't use all features in every tree
                         )
    return modelXGB