# Figure width in inches, approximately A4-width - 2*1.25in margin
xtick.color: 323034
ytick.color: 323034
text.color: 323034
lines.markeredgecolor: black
patch.facecolor        : bc80bd
patch.force_edgecolor  : True
patch.linewidth: 0.6
scatter.edgecolors: black
grid.color: b1afb5
axes.titlesize: 6
legend.title_fontsize: 6
xtick.labelsize: 5
ytick.labelsize: 5
axes.labelsize: 6
font.size: 6
axes.prop_cycle : (cycler('color', ['bc80bd' ,'fb8072', 'b3de69','fdb462','fccde5','8dd3c7','ffed6f','bebada','80b1d3', 'ccebc5', 'd9d9d9']))
# mathtext.fontset: stix / stixsans
# mathtext.fontset: cm
# mathtext.rm : serif
lines.linewidth: 0.6
legend.frameon: True
legend.framealpha: 0.8
legend.fontsize: 6
legend.edgecolor: 0.9
legend.borderpad: 0.2
legend.columnspacing: 1.5
legend.labelspacing:  0.4
text.usetex: False
axes.titlelocation: center
axes.formatter.use_mathtext: True
axes.autolimit_mode: round_numbers
axes.labelpad: 3
axes.formatter.limits: -4, 4
axes.labelcolor: black
axes.edgecolor: black
axes.linewidth: 0.7
axes.spines.right : False
axes.spines.top : False
axes.grid: False
figure.titlesize: 6
figure.dpi: 300