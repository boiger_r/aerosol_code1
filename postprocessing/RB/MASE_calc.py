import os
import numpy as np
import pandas as pd
import argparse

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('--test_data_dir', type = str, default ='none', help = 'Path to the folder containing test data')
	parser.add_argument('--dir', type = str, default ='none', help = 'Path to the folder, where models are stored')

	args = parser.parse_args()
	directory = args.dir
	test_data_directory = args.test_data_dir
    
	test_datafile = test_data_directory +'test_dataset.hdf5'

	plot_dir = directory+"/plots/model"

	dvar = pd.read_hdf(f'{directory}/model/pred.hdf5',key = 'inv')
	qoi = pd.read_hdf(f'{directory}/model/pred.hdf5',key = 'fw')
	dvar_test = pd.read_hdf(f'{test_datafile}','dvar')
	qoi_test = pd.read_hdf(f'{test_datafile}','qoi')    
	dvar_test.reset_index(drop=True, inplace=True)
	qoi_test.reset_index(drop=True, inplace=True)
    
	def MASE(test_data, predicted_data):
		n = test_data.shape[0]
		difference = (np.abs(np.diff(test_data,axis = 0)))
		summe = np.sum(difference, axis =0 )/(n-1)    
		errors_dvar = np.abs(test_data-predicted_data)    
		return errors_dvar.mean()/summe
    
	P11_columns = []
	P12_columns = []
	for name in qoi_test.columns:
		if 'P11' in name:
			P11_columns.append(name)
		elif 'P12' in name:
			P12_columns.append(name)
            
	MASE_qoi =MASE(qoi_test, qoi)
	MASE_dvar = MASE(dvar_test, dvar)
	MASE_P11 = MASE_qoi[P11_columns].mean()
	MASE_P12 = MASE_qoi[P12_columns].mean()
	store_results = pd.DataFrame()
	store_results['MASE_P11']=[MASE_P11]
	store_results['MASE_P12']=[MASE_P12]
	store_results[dvar.columns]=MASE_dvar
 
	store_results.to_csv(f'{plot_dir}/MASE.csv')
