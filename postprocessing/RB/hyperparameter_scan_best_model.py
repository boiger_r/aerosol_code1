import os
import shutil
import pandas as pd
import numpy as np
import sklearn
import argparse

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('--dir', type = str, default ='none', help = 'Path to the folder containing hyperparameter scan results')
	args = parser.parse_args()
	result_dir= args.dir
	model_dir = result_dir +'/model'
	if not os.path.exists(model_dir):
		os.makedirs(model_dir)
	df = pd.read_csv(f'{result_dir}/results.csv', index_col=0)
	config_columns = [col for col in df.columns if col.startswith('config/')]
	config_column_labels = {col: col.replace('config/', '') for col in config_columns if col != 'datafile'}
	best_config = df.sort_values('r2_val_dvar', ascending=False).iloc[0, :]
	best_model_dir = f'{best_config["logdir"]}/surrogate_model'
	shutil.copytree(best_model_dir, f'{model_dir}/invertible_model1')
	best_config_df = pd.DataFrame(best_config)
	best_config_df = best_config_df.T
	best_config_df.to_csv(f'{model_dir}/configs_scan.csv')

