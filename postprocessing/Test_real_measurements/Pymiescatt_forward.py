import numpy as np
import pandas as pd
import time
import PyMieScatt as ps

def lognormal(D_array, D_median, GSD, Ntot):  # dn/dD
    n = (Ntot/(np.sqrt(2*np.pi)*(np.log(GSD)))/D_array) * np.exp(-(np.log(D_array)-np.log(D_median))**2/(2*(np.log(GSD)**2)))   # nm-1 * cm-3
    return n

def run_pymiescatt(R_median, GSD, N_tot, ri_n, ri_k, wvl, angles, D_array):
    '''
    R_median in um (NOTE: this is the median radius of the NUMBER size distribution!)
    N_tot in cm-3
    ri_k as positive float number
    wvl in um
    angles is the array of angles
    D_array is the diameter array in nm
    '''
    lambd = wvl *1e3   # from um to nm
    m = ri_n + 1j * ri_k
    mu = np.cos(angles * np.pi / 180)
    
    n = lognormal(D_array, R_median*2 * 1e3, GSD, N_tot)     
        
    S11 = []
    S12 = []
    for diam in D_array:
        x = diam * np.pi / lambd
        theta, SL, SR, S11_temp = ps.ScatteringFunction(m, lambd, diam,
                                                        minAngle=angles.min(), maxAngle=angles.max(),
                                                        angularResolution=angles[1]-angles[0],
                                                        normalization=None)
    
        S11.append(S11_temp)
        S12.append(0.5*(SR-SL))
    S11 = np.array(S11).T
    S12 = np.array(S12).T  
            
    F11 = np.trapz(S11 * n * (lambd/(2*np.pi))**2. * 1e-6, x=D_array)
    F12 = np.trapz(S12 * n * (lambd/(2*np.pi))**2. * 1e-6, x=D_array)

    PPF = - F12 / F11
      
    pymiescatt = np.concatenate((F11,PPF)) 
    return pymiescatt



    


def pymiescatt_forward_prediction(parameters):
    angles = np.arange(0,180,1)
    wvl =  0.532
    V_tot, R_median_V, GSD, ri_n, ri_k = parameters.iloc[0].values
    D_median_V = R_median_V*2  # [nm]
    D_median_N = (np.exp(np.log(D_median_V) - (3*np.log(GSD)**2)))       
    testnr = 10
    lower = D_median_V/GSD/testnr
    upper = D_median_V*GSD*testnr

    D_array = np.logspace(np.log10(lower),np.log10(upper),301)

    N_tot = 6 / np.pi * V_tot*1e9 / (np.exp((3*np.log(D_median_N)) + (4.5*np.log(GSD)**2))) # [cm^-3]
    datalist1 = run_pymiescatt(D_median_N / 2 * 1e-3, GSD, N_tot, ri_n, ri_k, wvl, angles, D_array)
    
    return datalist1
