import os
import argparse
import numpy as np
import pandas as pd
import scipy
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
import tensorflow as tf
from sklearn.model_selection import train_test_split, KFold
from mllib.model import KerasSurrogate
from helper_functions.scan_helper_functions import AdjustedRSquared
from helper_functions.ml_helper_functions import RSquaredSeparated, AdjustedRSquaredSeparated
from helper_functions.invertible_neural_network import InvertibleNetworkSurrogate
#from helper_functions.invertible_neural_network_positive import InvertibleNetworkSurrogate

from sklearn.metrics import mean_absolute_error, mean_squared_error, explained_variance_score#,mean_absolute_percentage_error



seed = 49857
tf.random.set_seed(seed)
np.random.seed(seed)


def wmape(actual, pred):
    wMAPE = np.sum(np.abs(actual-pred))/np.sum(np.abs(actual))*100
    return wMAPE



parser = argparse.ArgumentParser()
parser.add_argument('--test_file',help = 'datafile, where the test data are stored')
parser.add_argument('--directory',help = 'directory, where the model is stored')
parser.add_argument('--model_name',help='model_name')

# +
args = parser.parse_args()
test_datafile = args.test_file
#model_name = 'invertible_model'
model_name = args.model_name
directory = args.directory
# create a folder to store the plots
plot_dir = directory +'/plots/model'+model_name

if not os.path.exists(plot_dir):
    os.makedirs(plot_dir)

# the folder, where the NN model is stored
model_dir = directory + '/models'

# Load the model
custom_objects = {'AdjustedRSquared': AdjustedRSquared}

kwargs = {'custom_objects': custom_objects,'compile': False}

surr = InvertibleNetworkSurrogate.load(model_dir, model_name, model_kwargs=kwargs)

# Load the test data
dvar_test = pd.read_hdf(test_datafile,key = 'dvar')
qoi_test = pd.read_hdf(test_datafile,key = 'qoi')
dvar_test.reset_index(drop=True, inplace=True)
qoi_test.reset_index(drop=True, inplace=True)

qoi_columns = qoi_test.columns
dvar_columns = dvar_test.columns

## Prediction
# forward predicition
qoi_pred_fw = surr.predict(dvar_test.values)
# make a pandas dataframe of the predicted values
qoi_pred_fw = pd.DataFrame(data=qoi_pred_fw, columns=qoi_columns)
F11_list = [x for x in qoi_pred_fw.columns if 'F11' in x]
PPF_list = [x for x in qoi_pred_fw.columns if 'PPF' in x]

# for comparing the predicted with the actual test data, redo the 
# manual preprocessing adapt!!!
qoi_pred_plot_fw = qoi_pred_fw.copy()
qoi_pred_plot_fw[F11_list] = np.exp(qoi_pred_plot_fw[F11_list])
qoi_test_plot = qoi_test.copy()
qoi_test_plot[F11_list]=np.exp(qoi_test_plot[F11_list])

# inverse prediction
# adjust the batch_size to what you used during training, 
# n_tries gives you the number of predicted dvar, from where the best is taken
dvar_pred_iv_original = surr.sample_n_tries(qoi_test.values,batch_size = 8, n_tries = 5)
# make a pandas dataframe of the predicted parameters
dvar_pred_iv = pd.DataFrame(data = dvar_pred_iv_original, columns = dvar_test.columns)



# for comparing the predicted with the actual test data, redo the 
# manual preprocessing - adapt!!!
dvar_test['RmedianV'] = np.exp(dvar_test['RmedianV'])
dvar_test['GSD']=np.exp(dvar_test['GSD'])+1
dvar_test['n'] = np.exp(dvar_test['n'])+1.33
dvar_test['k']=np.exp(dvar_test['k'])

dvar_pred_iv['RmedianV'] = np.exp(dvar_pred_iv['RmedianV'])
dvar_pred_iv['GSD']=np.exp(dvar_pred_iv['GSD'])+1
dvar_pred_iv['n'] = np.exp(dvar_pred_iv['n'])+1.33
dvar_pred_iv['k']=np.exp(dvar_pred_iv['k'])

## Calculate metrics

# R2
qoi_test_copy = qoi_test_plot.copy()
qoi_pred_fw_copy  = qoi_pred_plot_fw.copy()

metric_fw = RSquaredSeparated()

r2_adj_fw = metric_fw.call(qoi_test_copy.values, qoi_pred_fw_copy.values).numpy()
r2_adj_fw = pd.Series(data=r2_adj_fw, index=qoi_columns)
r2_adj_fw = np.round(r2_adj_fw, decimals=2)
r2_adj_fw = pd.DataFrame(r2_adj_fw).T

dvar_test_copy = dvar_test.copy()
dvar_pred_iv_copy = dvar_pred_iv.copy()

metric_iv = RSquaredSeparated()

r2_adj_iv = metric_iv.call(dvar_test_copy.values, dvar_pred_iv_copy.values).numpy()
r2_adj_iv = pd.Series(data=r2_adj_iv, index=dvar_columns)
r2_adj_iv = np.round(r2_adj_iv, decimals=2)
r2_adj_iv = pd.DataFrame(r2_adj_iv).T

abs_error_fw = np.abs( qoi_test_copy - qoi_pred_fw_copy)
rel_error_fw = abs_error_fw / (qoi_test_copy) * 100.
rel_error_fw = pd.DataFrame(rel_error_fw, columns=qoi_columns).abs()

abs_error_table_fw =  np.round(abs_error_fw.quantile([0.5, 0.75, 0.9, 0.95, 0.99]).rename(index={
    0.5: '50%',
    0.75: '75%',
    0.9: '90%',
    0.95: '95%',
    0.99: '99%',
}), decimals=2)

rel_error_table_fw = np.round(rel_error_fw.quantile([0.5, 0.75, 0.9, 0.95, 0.99]).rename(index={
    0.5: '50%',
    0.75: '75%',
    0.9: '90%',
    0.95: '95%',
    0.99: '99%',
}), decimals=2)

F11_532_columns = []
PPF_532_columns = []
for name in qoi_columns:
    if 'F11' in name:
        F11_532_columns.append(name)
    if 'PPF' in name:
        PPF_532_columns.append(name)

abs_error_table_fw_F11 = abs_error_table_fw[F11_532_columns]

abs_error_table_fw_PPF = abs_error_table_fw[PPF_532_columns]
rel_error_table_fw_F11 = rel_error_table_fw[F11_532_columns]

rel_error_table_fw_PPF = rel_error_table_fw[PPF_532_columns]

abs_error_iv = np.abs(dvar_test_copy-dvar_pred_iv_copy)
rel_error_iv = abs_error_iv / (dvar_test_copy) * 100.
rel_error_iv = pd.DataFrame(rel_error_iv, columns=dvar_columns).abs()

abs_error_table_iv = np.round(abs_error_iv.quantile([0.5, 0.75, 0.9, 0.95, 0.99]).rename(index={
    0.5: '50%',
    0.75: '75%',
    0.9: '90%',
    0.95: '95%',
    0.99: '99%',
}), decimals=2)
rel_error_table_iv = np.round(rel_error_iv.quantile([0.5, 0.75, 0.9, 0.95, 0.99]).rename(index={
    0.5: '50%',
    0.75: '75%',
    0.9: '90%',
    0.95: '95%',
    0.99: '99%',
}), decimals=2)


wMAPE_qoi = wmape(qoi_test_copy, qoi_pred_fw_copy)
wMAPE_qoi = pd.DataFrame(wMAPE_qoi).T

wMAPE_dvar = wmape(dvar_test_copy, dvar_pred_iv_copy)
wMAPE_dvar = pd.DataFrame(wMAPE_dvar).T

presentation_plot_index = [0,100,200,500,600,800,1000] 

fig, ax = plt.subplots(figsize = (10,10))
for index in presentation_plot_index:
    ax.plot(np.arange(0,len(qoi_test_copy[F11_532_columns].iloc[index])),qoi_test_copy[F11_532_columns].iloc[index],'b')
    ax.plot(np.arange(0,len(qoi_test_copy[F11_532_columns].iloc[index])),qoi_pred_fw_copy[F11_532_columns].iloc[index],'r')

ax.set_xlabel('Scattering angle',fontsize = 25)
ax.set_ylabel('F11',fontsize = 25)
ax.tick_params(labelsize = 16)    
fig.tight_layout() 
ax.set_yscale('log')
ax.set_title( 'Comparison Test and Prediction',fontsize = 25)
ax.legend(['test data', 'predicted test data'], fontsize = 16)
fig.savefig(f'{plot_dir}/phase_function.jpg')
fig, ax = plt.subplots(figsize = (10,10))
for index in presentation_plot_index:
    ax.plot(np.arange(0,len(qoi_test_copy[PPF_532_columns].iloc[index])),qoi_test_copy[PPF_532_columns].iloc[index],'b')
    ax.plot(np.arange(0,len(qoi_test_copy[PPF_532_columns].iloc[index])),qoi_pred_fw_copy[PPF_532_columns].iloc[index],'r')



ax.set_xlabel('Scattering angle',fontsize = 25)
ax.set_ylabel('PPF',fontsize = 25)
ax.tick_params(labelsize = 16)    
fig.tight_layout() 
ax.set_title( 'Comparison Test and Prediction',fontsize = 25)
ax.legend(['test data', 'predicted test data'], fontsize = 16)
fig.savefig(f'{plot_dir}/polarized_phase_function.jpg')

def lognormal(D_array, D_median, GSD, Ntot):  # dn/dD
    n = (Ntot/(np.sqrt(2*np.pi)*(np.log(GSD)))/D_array) * np.exp(-(np.log(D_array)-np.log(D_median))**2/(2*(np.log(GSD)**2)))   # nm-1 * cm-3
    return n

dvar_test['DmedianV']=dvar_test['RmedianV']*2*1e-3 #[um]
dvar_test['DmedianN']=(np.exp(np.log(dvar_test['DmedianV']) - (3*np.log(dvar_test['GSD'])**2))) *1e3   #[nm]
dvar_test['Ntot'] = 6 / np.pi * dvar_test['Vtot']*1e9 / (np.exp((3*np.log(dvar_test['DmedianN'])) + (4.5*np.log(dvar_test['GSD'])**2))) # [cm^-3]
 
dvar_pred_iv['DmedianV']=dvar_pred_iv['RmedianV']*2*1e-3 #[um]
dvar_pred_iv['DmedianN']=(np.exp(np.log(dvar_pred_iv['DmedianV']) - (3*np.log(dvar_pred_iv['GSD'])**2))) *1e3 #[nm] 
dvar_pred_iv['Ntot'] = 6 / np.pi * dvar_pred_iv['Vtot']*1e9 / (np.exp((3*np.log(dvar_pred_iv['DmedianN'])) + (4.5*np.log(dvar_pred_iv['GSD'])**2))) # [cm^-3]
D_array = np.logspace(1, 4.5, 200)  # [nm]

fig, ax = plt.subplots(figsize = (10,10))



for i  in presentation_plot_index:
    
    pdf1 = lognormal(D_array, dvar_test['DmedianN'].loc[i], dvar_test['GSD'].loc[i], dvar_test['Ntot'].loc[i])
    pdf2 = lognormal(D_array, dvar_pred_iv['DmedianN'].loc[i], dvar_pred_iv['GSD'].loc[i], dvar_pred_iv['Ntot'].loc[i])
    
    ax.plot(D_array, pdf1*4/3.*np.pi*D_array**3., '*g', label = 'Simulated test data')
    
    ax.plot(D_array, pdf2*4/3.*np.pi*D_array**3., 'b', label = 'Retrieved test data')
    
    
ax.legend(['Predicted test data', 'test data'], fontsize = 16)
ax.legend(fontsize = 16)   

ax.set_xlabel('Particle Radius [$\mu$m]',fontsize = 25)
ax.set_ylabel('Concentration ',fontsize = 25)
ax.tick_params(labelsize = 16)    
ax.set_title( 'Comparison Simulation and Prediction',fontsize = 25)
fig.align_labels()

plt.xscale('log')
#plt.yscale('log')
fig.tight_layout() 
fig.savefig(f'{plot_dir}/size_distribution.jpg')

results_dataframe = pd.DataFrame(columns = ['F11','PPF','Vtot','RmedianV','GSD','n','k'], index = ['abs_err50%','abs_err75%','abs_err90%','abs_err95%','abs_err99%','rel_err50%','rel_err75%','rel_err90%','rel_err95%','rel_err99%','wmape','R2'])
results_dataframe['F11'].iloc[0:5]= abs_error_table_fw_F11.max(axis = 1)
results_dataframe['PPF'].iloc[0:5]= abs_error_table_fw_PPF.max(axis = 1)

results_dataframe['Vtot'].iloc[0:5]=abs_error_table_iv['Vtot']
results_dataframe['RmedianV'].iloc[0:5]=abs_error_table_iv['RmedianV']
results_dataframe['GSD'].iloc[0:5]=abs_error_table_iv['GSD']
results_dataframe['n'].iloc[0:5]=abs_error_table_iv['n']
results_dataframe['k'].iloc[0:5]=abs_error_table_iv['k']

results_dataframe['F11'].iloc[5:10]= rel_error_table_fw_F11.max(axis = 1)
results_dataframe['PPF'].iloc[5:10]= rel_error_table_fw_PPF.max(axis = 1)

results_dataframe['Vtot'].iloc[5:10]=rel_error_table_iv['Vtot']
results_dataframe['RmedianV'].iloc[5:10]=rel_error_table_iv['RmedianV']
results_dataframe['GSD'].iloc[5:10]=rel_error_table_iv['GSD']
results_dataframe['n'].iloc[5:10]=rel_error_table_iv['n']
results_dataframe['k'].iloc[5:10]=rel_error_table_iv['k']

results_dataframe['F11'].iloc[10]= np.round(wMAPE_qoi[F11_532_columns].mean().mean(), decimals = 2)
results_dataframe['PPF'].iloc[10]= np.round(wMAPE_qoi[PPF_532_columns].mean().mean(), decimals = 2)

results_dataframe['Vtot'].iloc[10]=np.round(wMAPE_dvar['Vtot'].values[0], decimals = 2)
results_dataframe['RmedianV'].iloc[10]=np.round(wMAPE_dvar['RmedianV'].values[0], decimals = 2)
results_dataframe['GSD'].iloc[10]=np.round(wMAPE_dvar['GSD'].values[0], decimals = 2)
results_dataframe['n'].iloc[10]=np.round(wMAPE_dvar['n'].values[0], decimals = 2)
results_dataframe['k'].iloc[10]=np.round(wMAPE_dvar['k'].values[0], decimals = 2)

results_dataframe['F11'].iloc[11]= np.round(r2_adj_fw[F11_532_columns].mean(axis = 1).values[0], decimals = 2)
results_dataframe['PPF'].iloc[11]= np.round(r2_adj_fw[PPF_532_columns].mean(axis = 1).values[0], decimals = 2)

results_dataframe['Vtot'].iloc[11]=r2_adj_iv['Vtot'].values[0]
results_dataframe['RmedianV'].iloc[11]=r2_adj_iv['RmedianV'].values[0]
results_dataframe['GSD'].iloc[11]=r2_adj_iv['GSD'].values[0]
results_dataframe['n'].iloc[11]=r2_adj_iv['n'].values[0]
results_dataframe['k'].iloc[11]=r2_adj_iv['k'].values[0]


plot_dir

dvar_pred_iv.to_hdf(f'{plot_dir}/test_data_prediction.h5',key='dvar')    
qoi_pred_fw_copy.to_hdf(f'{plot_dir}/test_data_prediction.h5',key='qoi')    
results_dataframe.to_csv(f'{plot_dir}/test_data_performance.csv')    

# -


