import os
import argparse
import numpy as np
import pandas as pd
import scipy
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
import tensorflow as tf
from sklearn.model_selection import train_test_split, KFold
from mllib.model import KerasSurrogate
from helper_functions.scan_helper_functions import AdjustedRSquared
from helper_functions.ml_helper_functions import RSquaredSeparated, AdjustedRSquaredSeparated
from helper_functions.invertible_neural_network import InvertibleNetworkSurrogate


from sklearn.metrics import mean_absolute_error, mean_squared_error, explained_variance_score

import PyMieScatt as pms

seed = 49857
tf.random.set_seed(seed)
np.random.seed(seed)

from analyzing_functions import *


parser = argparse.ArgumentParser()
#parser.add_argument('--data_file',help = 'datafile, where the test data are stored')
parser.add_argument('--directory',help = 'directory, where the model is stored')
parser.add_argument('--model_name',help='model_name')

pymiescatt_df = pd.read_hdf('/data/project/general/aerosolretriev/aerosol_data/dataset_20_10_2022/data_meas_pms.h5',key='qoi')
datafile= '/data/project/general/aerosolretriev/aerosol_data/real_measurement_data_october2022/uneph_data_20221024_values.h5'


args = parser.parse_args()
#datafile = args.data_file
#model_name = 'invertible_model'
model_name = args.model_name
directory = args.directory
#print(directory)
# create a folder to store the plots
plot_dir = directory +'/plots/model'+model_name

if not os.path.exists(plot_dir):
    os.makedirs(plot_dir)

# the folder, where the NN model is stored
model_dir = directory + '/models'

# Load the model
custom_objects = {'AdjustedRSquared': AdjustedRSquared}

kwargs = {'custom_objects': custom_objects,'compile': False}

surr = InvertibleNetworkSurrogate.load(model_dir, model_name, model_kwargs=kwargs)


qoi_test1 = pd.read_hdf(datafile,'qoi')
dvar_test1 = pd.read_hdf(datafile,'dvar')

qoi_test1_2predict= qoi_test1.copy()
qoi_t1_2predict= qoi_test1.copy()

F11_list_test = [x for x in qoi_test1.columns if 'F11' in x]
PPF_list_test = [x for x in qoi_test1.columns if 'PPF' in x]

angles1 = np.arange(10,86)
angles2 = np.arange(95,171)
angles = np.concatenate([angles1, angles2])

qoi_t1_2predict[F11_list_test]=np.log(qoi_t1_2predict[F11_list_test]/4/np.pi)

dvar_t1_2predict = surr.sample_n_tries(qoi_t1_2predict.values,batch_size = 8, n_tries =32)
dvar_t1_2predict = pd.DataFrame(data = dvar_t1_2predict, columns = dvar_test1.columns)
dvar_t1_2predict_2qoi = dvar_t1_2predict.copy()

dvar_t1_2predict['Vtot']=np.exp(dvar_t1_2predict['Vtot'])
dvar_t1_2predict['RmedianV']=np.exp(dvar_t1_2predict['RmedianV'])
dvar_t1_2predict['GSD']=np.exp(dvar_t1_2predict['GSD'])+1
dvar_t1_2predict['n']=np.exp(dvar_t1_2predict['n'])+1.33
dvar_t1_2predict['k']=np.exp(dvar_t1_2predict['k'])

dvar_t1_2predict['RmedianV']=dvar_t1_2predict['RmedianV']*1e-3

qoi_t1_2predict_pred = surr.predict(dvar_t1_2predict_2qoi.values)
qoi_t1_2predict_pred = pd.DataFrame(data=qoi_t1_2predict_pred, columns=qoi_test1.columns)
F11_list = F11_list_test
PPF_list = PPF_list_test
qoi_t1_2predict_pred[F11_list]=np.exp(qoi_t1_2predict_pred[F11_list])

D_array = np.logspace(0.1, 5, 300)  # [nm] base 10
theta = np.arange(0,181,1)

newcolumns = []
F11_newcolumns = []
PPF_newcolumns = []

for i in range(0,181):
    newcolumns.append('F11_'+str(i))
    F11_newcolumns.append('F11_'+str(i))
for j in range(0,181):
    newcolumns.append('PPF_'+str(j))
    PPF_newcolumns.append('PPF_'+str(j))

theta_meas = []
for i in range(10,86):
    theta_meas.append(i)
for i in range(95,171):
    theta_meas.append(i)

fig, axs = plt.subplots(ncols=2, nrows=14, figsize=(10, 60), constrained_layout=True)

for index in range(14):
    axs[index,0].set_title('Measurement index: '+str(index))

    pms_hdf_plot, = axs[index,0].plot(theta, pymiescatt_df[F11_newcolumns].loc[index], ls='--', lw='1', c='r', alpha=0.8, label='PyMieScatt from hdf param')
    pms_hdf_plot, = axs[index,1].plot(theta, pymiescatt_df[PPF_newcolumns].loc[index], ls='--', lw='1', c='r', alpha=0.8, label='PyMieScatt from hdf param')

    meas_hdf_plot, = axs[index,0].plot(theta_meas, qoi_test1[F11_list_test].loc[index].values/4/np.pi, c= 'b', label = 'uneph')
    meas_hdf_plot, = axs[index,1].plot(theta_meas, qoi_test1[PPF_list_test].loc[index].values, c= 'b', label = 'uneph')
    
    meas_hdf_pred, = axs[index,0].plot(theta_meas, qoi_t1_2predict_pred[F11_list].loc[index].values,ls='-.', c= 'k', label = 'INN')
    meas_hdf_pred, = axs[index,1].plot(theta_meas, qoi_t1_2predict_pred[PPF_list].loc[index].values,ls='-.', c= 'k', label = 'INN')

    axs[index,0].set_ylabel('PF [$Mm^{-1}$]')
    axs[index,1].set_ylabel('PPF [-]')
    axs[index,1].set_ylim(-1.1, 1.1)
    axs[index,0].set_yscale('log')
    for ax in axs.reshape(-1):
        ax.set_xlabel('Angle')
        ax.legend(handles=[pms_hdf_plot,meas_hdf_plot,meas_hdf_pred])
        ax.grid(which='both')
fig.savefig(f'{plot_dir}/pymiescatt_measurements_pred.jpg')

fig, axs = plt.subplots(ncols=1, nrows=14, figsize=(5, 60), constrained_layout=True)

for index in range(14):
    param_tmp = dvar_test1.iloc[index]
    DmedianV_tmp = param_tmp['RmedianV']*2  # [um]
    GSD_tmp = param_tmp['GSD']
    Vtot_tmp = param_tmp['Vtot']
    DmedianN_tmp = (np.exp(np.log(DmedianV_tmp) - (3*np.log(GSD_tmp)**2))) *1e3   # [nm]
    Ntot_tmp = 6 / np.pi * Vtot_tmp*1e9 / (np.exp((3*np.log(DmedianN_tmp)) + (4.5*np.log(GSD_tmp)**2))) # [cm^-3]
    axs[index].plot(D_array, lognormal(D_array,DmedianN_tmp,GSD_tmp,Ntot_tmp ), c='r', ls='--', lw=2, label='measurement')
                    
                    
    param_tmp_pred = dvar_t1_2predict.iloc[index]
    DmedianV_tmp_pred = param_tmp_pred['RmedianV']*2  # [um]
    GSD_tmp_pred = param_tmp_pred['GSD']
    Vtot_tmp_pred = param_tmp_pred['Vtot']
    DmedianN_tmp_pred = (np.exp(np.log(DmedianV_tmp_pred) - (3*np.log(GSD_tmp_pred)**2))) *1e3   # [nm]
    Ntot_tmp_pred = 6 / np.pi * Vtot_tmp_pred*1e9 / (np.exp((3*np.log(DmedianN_tmp_pred)) + (4.5*np.log(GSD_tmp_pred)**2))) # [cm^-3]
    axs[index].plot(D_array, lognormal(D_array,DmedianN_tmp_pred,GSD_tmp_pred,Ntot_tmp_pred ),ls='-.', c='k', lw=2, label='INN')
    axs[index].set_xscale('log')
    axs[index].legend(fontsize=8)
    axs[index].set_xlim(50, )
    axs[index].grid(which='both')
    axs[index].set_title('Measurement index: '+str(index)) 
fig.savefig(f'{plot_dir}/pymiescatt_measurements_size_distr.jpg')

#compute performance
abs_err_dvar = (dvar_test1 - dvar_t1_2predict).abs()
rel_err_dvar = abs_err_dvar/dvar_test1.abs()
abs_error_qoi_F11 = np.abs(qoi_test1[F11_list_test].values-qoi_t1_2predict_pred[F11_list].values)
abs_error_qoi_F11 = pd.DataFrame(data = abs_error_qoi_F11,columns = qoi_t1_2predict_pred[F11_list].columns )

abs_error_qoi_PPF = np.abs(qoi_test1[PPF_list_test].values-qoi_t1_2predict_pred[PPF_list].values)
abs_error_qoi_PPF = pd.DataFrame(data = abs_error_qoi_PPF,columns = qoi_t1_2predict_pred[PPF_list].columns )

rel_error_qoi_F11 = abs_error_qoi_F11.values/np.abs(qoi_test1[F11_list_test].values)
rel_error_qoi_F11 = pd.DataFrame(data =rel_error_qoi_F11, columns = qoi_t1_2predict_pred[F11_list].columns)

rel_error_qoi_PPF = abs_error_qoi_PPF.values/np.abs(qoi_test1[PPF_list_test].values)
rel_error_qoi_PPF = pd.DataFrame(data =rel_error_qoi_PPF, columns = qoi_t1_2predict_pred[PPF_list].columns)


results_dataframe = pd.DataFrame(columns = ['AE_F11','AE_PPF','AE_Vtot','AE_RmedianV','AE_GSD','AE_n','AE_k','RE_F11','RE_PPF','RE_Vtot','RE_RmedianV','RE_GSD','RE_n','RE_k'])
results_dataframe['AE_F11'] =abs_error_qoi_F11.mean(axis = 1)
results_dataframe['RE_F11'] =rel_error_qoi_F11.mean(axis = 1)
results_dataframe['AE_PPF'] =abs_error_qoi_PPF.mean(axis = 1)
results_dataframe['RE_PPF'] =rel_error_qoi_PPF.mean(axis = 1)
results_dataframe['AE_Vtot'] =abs_err_dvar['Vtot']
results_dataframe['RE_Vtot'] =rel_err_dvar['Vtot']
results_dataframe['AE_RmedianV'] =abs_err_dvar['RmedianV']
results_dataframe['RE_RmedianV'] =rel_err_dvar['RmedianV']
results_dataframe['AE_GSD'] =abs_err_dvar['GSD']
results_dataframe['RE_GSD'] =rel_err_dvar['GSD']
results_dataframe['AE_n'] =abs_err_dvar['n']
results_dataframe['RE_n'] =rel_err_dvar['n']
results_dataframe['AE_k'] =abs_err_dvar['k']
results_dataframe['RE_k'] =rel_err_dvar['k']


dvar_t1_2predict.to_hdf(f'{plot_dir}/measurement_data_prediction.h5',key='dvar')    
qoi_t1_2predict_pred.to_hdf(f'{plot_dir}/measurement_data_prediction.h5',key='qoi')    
np.round(results_dataframe, decimals=4).to_csv(f'{plot_dir}/measurement_data_performance.csv')    
np.round(dvar_t1_2predict, decimals = 4).to_csv(f'{plot_dir}/measurement_data_pred_dvar.csv') 





