import os
import argparse
import numpy as np
import pandas as pd
import scipy
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
import tensorflow as tf
from sklearn.model_selection import train_test_split, KFold
from mllib.model import KerasSurrogate
from helper_functions.scan_helper_functions import AdjustedRSquared
from helper_functions.ml_helper_functions import RSquaredSeparated, AdjustedRSquaredSeparated
from helper_functions.invertible_neural_network import InvertibleNetworkSurrogate


from sklearn.metrics import mean_absolute_error, mean_squared_error, explained_variance_score

import PyMieScatt as pms

def wmape(actual, pred):
    wMAPE = np.sum(np.abs(actual-pred))/np.sum(np.abs(actual))*100
    return wMAPE

'''def lognormal(D_array, D_median, GSD, Ntot):  # dn/dD
    n = (Ntot/(np.sqrt(2*np.pi)*(np.log(GSD)))/D_array) * np.exp(-(np.log(D_array)-np.log(D_median))**2/(2*(np.log(GSD)**2)))   # nm-1 * cm-3
    return n'''

def lognormal(D_array, D_median, GSD, Ntot):  # dn/dlogD
    n = (Ntot/(np.sqrt(2*np.pi)*(np.log10(GSD)))) * np.exp(-(np.log10(D_array)-np.log10(D_median))**2/(2*(np.log10(GSD)**2)))   # cm-3
    return n

def lognormal_fit(PSD, diameters, init_guess, fit_bounds):
    #fit_bounds=([0,0,2,0,0,3],[np.inf,1.25,5,np.inf,1.25,10])
    pu, pcovu = curve_fit(lognormal, diameters, PSD, p0=init_guess, bounds=fit_bounds, max_nfev=1e4)
    return(pu, pcovu)

def interp(df, new_index):
    """Return a new DataFrame with all columns values interpolated
    to the new_index values."""
    df_out = pd.DataFrame(index=new_index)
    df_out.index.name = df.index.name

    for colname, col in df.iteritems():
        df_out[colname] = np.interp(new_index, df.index, col)

    return df_out

def read_results(qoi, var='F11'):
    F11 = [x for x in qoi.index if var in x]
    index_list = [int(x[10:]) for x in F11]
    values = qoi[F11].values
    output = pd.DataFrame(index=index_list, columns=[var])
    output[var] = values
    return output

def run_pymiescatt(R_median, GSD, N_tot, ri_n, ri_k, wvl, angles, D_array):
    '''
    R_median in um (NOTE: this is the median radius of the NUMBER size distribution!)
    N_tot in cm-3
    ri_k as positive float number
    wvl in um
    angles is the array of angles
    D_array is the diameter array in nm
    '''
    lambd = wvl *1e3   # from um to nm
    m = ri_n + 1j * ri_k
    mu = np.cos(angles * np.pi / 180)
    
    n = lognormal(D_array, R_median*2 * 1e3, GSD, N_tot)/D_array / 2.303
    
    cscat = []
    for d in D_array:
            cscat_temp = pms.MieQ(m, lambd, d, asCrossSection=True)[1]
            cscat.append(cscat_temp)
    
    S11 = []
    S11_test = []
    S12 = []
    for diam in D_array:
        x = diam * np.pi / lambd
        theta, SL, SR, S11_temp = pms.ScatteringFunction(m, lambd, diam,
                                                        minAngle=angles.min(), maxAngle=angles.max(),
                                                        angularResolution=angles[1]-angles[0],
                                                        normalization=None)
        S11_test.append(S11_temp)
        # S11.append(S11_temp / np.trapz(S11_temp, x=np.flip(mu)) / (2*np.pi))
        qscat_temp = pms.MieQ(m, lambd, diam, asCrossSection=False)[1]
        S11.append(S11_temp / (qscat_temp * x**2 * np.pi))
        
        # S12.append( (0.5*(SR-SL)) / np.trapz(S11_temp, x=np.flip(mu)) / (2*np.pi))
        S12.append( (0.5*(SR-SL)) / (qscat_temp * x**2 * np.pi))
    S11 = np.array(S11).T
    # S11_test = np.array(S11_test).T
    S12 = np.array(S12).T            
         
    F11 = np.trapz(S11 * n * cscat * 1e-6, x=D_array)
    # F11_test = np.trapz(S11_test * n * (lambd / 4 / np.pi)**2 * 1e-6, x=D_array)
    F12 = np.trapz(S12 * n * cscat * 1e-6, x=D_array)
    PPF = - F12 / F11 
    
    pymiescatt = pd.DataFrame(index=angles, columns=['F11', 'F12', 'PPF'])
    pymiescatt['F11'] = F11
    # pymiescatt['F11_test'] = F11_test
    pymiescatt['F12'] = F12
    pymiescatt['PPF'] = PPF
    
    return pymiescatt
