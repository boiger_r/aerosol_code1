import os
import time
import argparse
import ray
from ray import tune
from helper_functions.scan_helper_functions import train_forward_model, train_invertible_model

if __name__ == '__main__':
    # Parse the command line arguments.
    parser = argparse.ArgumentParser()
    parser.add_argument('--result_dir',
                        help='Directory in which to save the results.')
    parser.add_argument('--model',
                        help='For which model to scan the parameters; can be "forward" or "invertible" (without the quotation marks)')
    parser.add_argument('--datafile',
                        help='Path to the HDF5 file containing the dataset.')
   # parser.add_argument('--forward_model_file',
    #                   help='Path to the folder, where the forward model is stored.')
    args = parser.parse_args()

    result_dir = args.result_dir
    history_dir = f'{result_dir}/histories'

    for directory in [result_dir, history_dir]:
        if not os.path.exists(directory):
            os.makedirs(directory)

    train_func = None
    if args.model == 'forward':
        train_func = train_forward_model
        config = {
#            'width': tune.grid_search([10,20,40]),
#            'depth': tune.grid_search([4, 5, 6, 7]),
#            'lr': tune.grid_search([0.0005]),
#            'batch_size': tune.grid_search([256]),
#            'datafile': args.datafile,
	     'width': tune.grid_search([10, 20, 40, 80, 160]),
            'depth': tune.grid_search([4, 5, 6, 7]),
            'lr': tune.grid_search([0.001, 0.0005, 0.0001]),
            'batch_size': tune.grid_search([128, 256]),
            'datafile': args.datafile,
        }
    elif args.model == 'invertible':
        train_func = train_invertible_model
        config = {
#            'n_blocks': tune.grid_search([3, 4, 5]),
#            'n_depth': tune.grid_search([3, 4]),
#            'n_width': tune.grid_search([10, 20, 30]),
#            'learning_rate': tune.grid_search([0.01, 0.05, 0.001, 0.005, 1e-4]),
#            'batch_size': tune.grid_search([128, 256]),
#            'datafile': args.datafile,
            'n_blocks': tune.grid_search([3, 4]),
            'n_depth': tune.grid_search([3, 4]),
            'n_width': tune.grid_search([10, 20, 30,40]),
            'learning_rate': tune.grid_search([0.001, 0.005,0.0001]),
            'batch_size': tune.grid_search([8,64,128]),
            'datafile': args.datafile,
        #    'forward_model_file': args.forward_model_file,
        }
    else:
        raise ValueError(f'Unknown model: {args.model}')

    ray.init()

    start = time.time()
    analysis = tune.run(train_func,
                        config=config,
                        local_dir=result_dir,
                        num_samples=1,
                        resources_per_trial={'cpu': 1},
                        queue_trials=False)

    # Save the training histories and the results.
    for key, df in analysis.trial_dataframes.items():
        ID = df['trial_id'].unique()[0]
        df.to_csv(f'{history_dir}/{ID}.csv')

    analysis.dataframe().to_csv(f'{result_dir}/results.csv')

    end = time.time()
    with open(f'{result_dir}/time_for_FW_scan.txt', 'w') as file:
        file.write(f'{end - start} s')
