import os
import time
import argparse
import ray
from ray import tune
import tensorflow as tf
import numpy as np
tf.keras.backend.set_floatx('float64')
from helper_functions.scan_helper_functions_configs import train_forward_model, train_invertible_model
from ray.tune.schedulers import AsyncHyperBandScheduler
from mllib.model import  AdaptiveMinMaxScaler, DummyPreprocessor, LogarithmTransform, StandardScaler


if __name__ == '__main__':
    # Parse the command line arguments.
    parser = argparse.ArgumentParser()
    parser.add_argument('--result_dir',
                        help='Directory in which to save the results.')
    parser.add_argument('--model',
                        help='For which model to scan the parameters; can be "forward"'
                        'or "invertible" (without the quotation marks)')
    parser.add_argument('--datafile',
                        help='Path to the HDF5 file containing the dataset.')
    args = parser.parse_args()

    result_dir = args.result_dir
    history_dir = f'{result_dir}/histories'

    for directory in [result_dir, history_dir]:
        if not os.path.exists(directory):
            os.makedirs(directory)

    train_func = None
    # Forward models
    if args.model == 'forward':
        train_func = train_forward_model
        learn = {'learn_rate' : tune.grid_search([0.0005])}
        config = {
            'width': tune.grid_search([20]),   #20,40,80
            'depth': tune.grid_search([3]),      # 40
            'lr': learn['learn_rate'],
            'batch_size': tune.grid_search([128]),  # 8,16,32,128,256
            'datafile': args.datafile,
            'loss' : 'MSE',
            'optimizer': tf.keras.optimizers.Adam(lr=learn['learn_rate']['grid_search'][0]),
            'preprocessor_x': AdaptiveMinMaxScaler(),
            'preprocessor_y': AdaptiveMinMaxScaler(),
            'epochs': tune.grid_search([100]),
            'training_repetitions': tune.grid_search([50]),
            'activation_function' : tune.grid_search(['relu'])
        }
    elif args.model == 'ASHA_forward':
        train_func = train_forward_model
        learn = {'learn_rate' : tune.uniform(0.001, 0.00001)}
        config = {
                'depth': tune.randint(2, 10),
                'width': tune.randint(10, 160),
                'lr': learn['learn_rate'],
                'batch_size': tune.choice([8,16,32,128,256]),
                'datafile': args.datafile,
                'loss' : 'MSE',
                'optimizer': tf.keras.optimizers.Adam(lr=learn['learn_rate']['grid_search'][0]),
                'preprocessor_x': AdaptiveMinMaxScaler(),
                'preprocessor_y': AdaptiveMinMaxScaler(),
                'epochs': tune.grid_search([100]),
                'training_repetitions': tune.grid_search([50]),
                'activation_function' : tune.grid_search(['relu'])
        }
        # AsyncHyperBand enables aggressive early stopping of bad trials.
        """ Note: During training of the forward model, it reports to ray every
            100 epochs. Therefore in the case of the maximum number of
            iterations, which is 50, 5000 epochs have beeen gone through."""
        scheduler = AsyncHyperBandScheduler(
            # 'training_iteration' is incremented every time `trainable.step` is called
            time_attr='training_iteration',
            # The training result objective value attribute. Stopping procedures will use this attribute.
            metric='MAE_val',
            # mode: {min, max}. Determines whether objective is minimizing or maximizing the metric attribute.
            mode='min',
            # max time units per trial. Trials will be stopped after max_t time units (determined by time_attr).
            max_t=50,
            # Only stop trials at least this old in time. The units are the same as the attribute named by time_attr.
            grace_period=10,
            # Used to set halving rate and amount. This is simply a unit-less scalar.
            reduction_factor=4,
            # Number of brackets. Each bracket has a different halving rate, specified by the reduction factor.
            brackets=1
            )
        # 'training_iteration' is incremented every time `trainable.step` is called
        # For each trial, stop when trial has reached 50 iterations
        stopping_criteria = {"training_iteration": 50}

    # Invertible models
    elif args.model == 'invertible':
        train_func = train_invertible_model
        learn = {'learn_rate' : 0.0005}
        optimizer_list = {
            'Adam': tf.keras.optimizers.Adam(learning_rate=learn['learn_rate'],
                                             beta_1=0.9, beta_2=0.999, epsilon=1e-07, amsgrad=False),
            'SGD': tf.keras.optimizers.SGD(learning_rate=learn['learn_rate'],
                                           momentum=0.0, nesterov=False),
            # Those requireing addons
        #    'AdamW': tfa.optimizers.AdamW(learning_rate=config['learning_rate'],
        #                                  weight_decay=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-07,
        #                                  amsgrad=False),
        #    'LAMB': tfa.optimizers.LAMB(learning_rate=config['learning_rate'],
        #                                weight_decay_rate=0.0, beta_1=0.9, beta_2=0.999, epsilon=1e-06),
        }
        config = {
                'n_blocks': tune.grid_search([5]),
                'n_depth': tune.grid_search([3]),
                'n_width': tune.grid_search([20]),
                'learning_rate': learn['learn_rate'],
                'batch_size': tune.grid_search([8]),
                'datafile': args.datafile,
                'weight_artificial': tune.grid_search([0.1]),
                'weight_reconstruction': tune.grid_search([1,10,20,40]),#0.05,100
                'weight_x': tune.grid_search([1,10,20,40]),#1,0.3,100,400
                'weight_y': tune.grid_search([1,10,20,40]),#1,0.3,100,400
                'weight_z': tune.grid_search([1]),#1,0.3,100,400
                'optimizer': optimizer_list['Adam'],
                'nominal_dimension' : 330,   #must be an even number and greater than the number of qoi
                'epochs' : 50,
                'preprocessor_x' : tune.grid_search([DummyPreprocessor()]),
                'preprocessor_y' : tune.grid_search([DummyPreprocessor()]),
                'activation_functions_inbetween' : 'relu',
                'activation_function_last_layer' : tune.choice(['linear'])
                #'optimizer' : 'Adam', # 'Adam', 'SGD', ['AdamW', 'LAMB']
        }
    elif args.model == 'ASHA_invertible':
        train_func = train_invertible_model
        learn = {'learn_rate': 0.0005 }# np.random.uniform(1e-5, 1e-1)}
        # Dictionary of optimizers to choose from, values given here a re default
        optimizer_list = {
            'Adam': tf.keras.optimizers.Adam(learning_rate=learn['learn_rate'],
                                             beta_1=0.9, beta_2=0.999, epsilon=1e-07, amsgrad=False),
            'SGD': tf.keras.optimizers.SGD(learning_rate=learn['learn_rate'],
                                           momentum=0.0, nesterov=False),
            # Those requireing addons
        #    'AdamW': tfa.optimizers.AdamW(learning_rate=config['learning_rate'],
        #                                  weight_decay=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-07,
        #                                  amsgrad=False),
        #    'LAMB': tfa.optimizers.LAMB(learning_rate=config['learning_rate'],
        #                                weight_decay_rate=0.0, beta_1=0.9, beta_2=0.999, epsilon=1e-06),
        }
        config = {
                'n_blocks': 5,#tune.grid_search([2,3,4,5,10]),  #tune.randint(2,6),
                'n_depth': 3,#tune.grid_search([2,3,5]),#tune.randint(3,6),
                'n_width': 20,#tune.grid_search([2,10,20,40]),#tune.randint(10, 40),
                #'learning_rate': tune.uniform(0.001, 0.0001),
                'batch_size': tune.choice([8]),
                'learning_rate': learn['learn_rate'],
                'datafile': args.datafile,
                'weight_artificial': 0.1, #tune.randint(0.05,1),
                'weight_reconstruction': 10,#tune.randint(1,100),
                'weight_x': tune.grid_search([1,10,20,30,40,50,100]),#tune.randint(1,100),
                'weight_y': tune.grid_search([1,10,20,30,40,50,100]),#tune.randint(1,100),
                'weight_z': 1,#tune.randint(1,100),
                'optimizer': optimizer_list['Adam'],
                #'optimizer': tf.keras.optimizers.Adam(lr=learn['learn_rate']['grid_search'][0]),
                'nominal_dimension' : 330,  #must be an even number and greater than the number of qoi
                'epochs' : 50,
                'preprocessor_x' : DummyPreprocessor(),
                'preprocessor_y' : DummyPreprocessor(),
                'activation_functions_inbetween' : 'relu',
                'activation_function_last_layer' : 'linear',
        }
        # AsyncHyperBand enables aggressive early stopping of bad trials.
        """ Note: During training of the inverse model, it reports to ray every
            epoch. Therefore in the case of the maximum number of
            iterations, which is 30, 30 epochs have beeen gone through.
            (@ Romana: I am unsure, wether this is intended,
            but I have adapted the code below acordingly)
        """
        scheduler = AsyncHyperBandScheduler(
            # 'training_iteration' is incremented every time `trainable.step` is called
            time_attr='training_iteration',
            # The training result objective value attribute. Stopping procedures will use this attribute.
            metric='MAE_val',
            # mode: {min, max}. Determines whether objective is minimizing or maximizing the metric attribute.
            mode='min',
            # max time units per trial. Trials will be stopped after max_t time units (determined by time_attr).
            max_t=30,
            # Only stop trials at least this old in time. The units are the same as the attribute named by time_attr.
            grace_period=5,
            # Used to set halving rate and amount. This is simply a unit-less scalar.
            reduction_factor=4,
            # Number of brackets. Each bracket has a different halving rate, specified by the reduction factor.
            brackets=1
            )
        # 'training_iteration' is incremented every time `trainable.step` is called
        # For each trial, stop when trial has reached 30 iterations
        stopping_criteria = {"training_iteration": 30}
    else:
        raise ValueError(f'Unknown model: {args.model}')

    ray.init()
    # Use Async Successive Halving, https://arxiv.org/abs/1810.05934
    if 'ASHA' in args.model:
        start = time.time()
        analysis = tune.run(train_func,
                            # num_samples are number of trials.
                            # Each trial is one instance of a Trainable.
                            #num_samples=95,  # 256 took about 8.5h for the forward model
                            name=args.model + '_id',
                            config=config,
                            local_dir=result_dir,
                            scheduler=scheduler,
                            stop=stopping_criteria,
                            #resources_per_trial={'cpu': 1,
                            #                     'gpu': 0},
                            queue_trials=False)
    else:  # No ASHA, plain vanilla
        start = time.time()
        analysis = tune.run(train_func,
                            # num_samples are number of trials.
                            # Each trial is one instance of a Trainable.
                            num_samples=1,
                            name=args.model + '_id',
                            config=config,
                            local_dir=result_dir,
                            resources_per_trial={'cpu': 1,
                                                 'gpu': 0},
                            queue_trials=False)

    # Save the training histories and the results.
    for key, df in analysis.trial_dataframes.items():
        ID = df['trial_id'].unique()[0]
        df.to_csv(f'{history_dir}/{ID}.csv')

    analysis.dataframe().to_csv(f'{result_dir}/results.csv')

    end = time.time()
    with open(f'{result_dir}/time_for_FW_scan.txt', 'w') as file:
        file.write(f'{end - start} s')
