#!/usr/bin/env python
# coding: utf-8
# %%

# %%


import os
import time
import argparse
import ray
from ray import tune
from helper_functions.autoencoder_helper import train_autoencoder


# %%


if __name__ == '__main__':
    # Parse the command line arguments.
    parser = argparse.ArgumentParser()
    parser.add_argument('--result_dir',
                        help='Directory in which to save the results.')
    parser.add_argument('--model',
                        help='For which model to scan the parameters; can be "forward" or "invertible" (without the quotation marks)')
    parser.add_argument('--datafile',
                        help='Path to the HDF5 file containing the dataset.')
    parser.add_argument('--forward_model_file',
                       help='Path to the folder, where the forward model is stored.')
    args = parser.parse_args()

    result_dir = args.result_dir
    history_dir = f'{result_dir}/histories'

    for directory in [result_dir, history_dir]:
        if not os.path.exists(directory):
            os.makedirs(directory)

    train_func = None
    if args.model == 'autoencoder':
        train_func = train_autoencoder
        config = {
            'alpha': tune.grid_search([1]),
            'learning_rate': tune.grid_search([0.001,0.01]),
            'datafile': args.datafile,
            'n_encode_layers': tune.grid_search([3,5,10,20]),
            'n_decode_layers': tune.grid_search([3,5,10,20]),
            'epochs': tune.grid_search([100]),
            'batch_size': tune.grid_search([8,64,128])
        }
    else:
        raise ValueError(f'Unknown model: {args.model}')

#    ray.init(memory=90000* 1024 * 1024, object_store_memory=50000 * 1024 * 1024)
    ray.init()
    start = time.time()
    analysis = tune.run(train_func,
                        config=config,
                        local_dir=result_dir,
                        num_samples=1,
                        resources_per_trial={'cpu': 1},
                        queue_trials=False)

    # Save the training histories and the results.
    for key, df in analysis.trial_dataframes.items():
        ID = df['trial_id'].unique()[0]
        df.to_csv(f'{history_dir}/{ID}.csv')

    analysis.dataframe().to_csv(f'{result_dir}/results.csv')

    end = time.time()
    with open(f'{result_dir}/time_for_FW_scan.txt', 'w') as file:
        file.write(f'{end - start} s')


# %%




