import os
import time
import argparse
import ray
from ray import tune
from helper_functions.scan_helper_functions import train_forward_model, train_invertible_model
from ray.tune.schedulers import AsyncHyperBandScheduler

if __name__ == '__main__':
    # Parse the command line arguments.
    parser = argparse.ArgumentParser()
    parser.add_argument('--result_dir',
                        help='Directory in which to save the results.')
    parser.add_argument('--model',
                        help='For which model to scan the parameters; can be "forward"'
                        'or "invertible" (without the quotation marks)')
    parser.add_argument('--datafile',
                        help='Path to the HDF5 file containing the dataset.')
    args = parser.parse_args()

    result_dir = args.result_dir
    history_dir = f'{result_dir}/histories'

    for directory in [result_dir, history_dir]:
        if not os.path.exists(directory):
            os.makedirs(directory)

    train_func = None
    # Forward models
    if args.model == 'forward':
        train_func = train_forward_model
        config = {
            'width': tune.grid_search([20,40,80]),
            'depth': tune.grid_search([3,4]),
            'lr': tune.grid_search([0.001,0.0001]),
            'batch_size': tune.grid_search([8,16,32,128,256]),
            'datafile': args.datafile,
        }
    elif args.model == 'ASHA_forward':
        train_func = train_forward_model
        config = {
                'depth': tune.randint(2, 10),
                'width': tune.randint(10, 160),
                'lr': tune.uniform(0.001, 0.00001),
                'batch_size': tune.choice([8,16,32,128,256]),
                'datafile': args.datafile,
        }
        # AsyncHyperBand enables aggressive early stopping of bad trials.
        """ Note: During training of the forward model, it reports to ray every
            100 epochs. Therefore in the case of the maximum number of
            iterations, which is 50, 5000 epochs have beeen gone through."""
        scheduler = AsyncHyperBandScheduler(
            # 'training_iteration' is incremented every time `trainable.step` is called
            time_attr='training_iteration',
            # The training result objective value attribute. Stopping procedures will use this attribute.
            metric='MAE_val',
            # mode: {min, max}. Determines whether objective is minimizing or maximizing the metric attribute.
            mode='min',
            # max time units per trial. Trials will be stopped after max_t time units (determined by time_attr).
            max_t=50,
            # Only stop trials at least this old in time. The units are the same as the attribute named by time_attr.
            grace_period=10,
            # Used to set halving rate and amount. This is simply a unit-less scalar.
            reduction_factor=4,
            # Number of brackets. Each bracket has a different halving rate, specified by the reduction factor.
            brackets=1
            )
        # 'training_iteration' is incremented every time `trainable.step` is called
        # For each trial, stop when trial has reached 50 iterations
        stopping_criteria = {"training_iteration": 50}

    # Invertible models
    elif args.model == 'invertible':
        train_func = train_invertible_model
        config = {
                'n_blocks': tune.grid_search([2,3,4]),
                'n_depth': tune.grid_search([2,4]),
                'n_width': tune.grid_search([10,20]),
                'learning_rate': tune.grid_search([0.001,0.0001]),
                'batch_size': tune.grid_search([8,16,32,64]),
                'datafile': args.datafile,
        }
    elif args.model == 'invertible_test':
        train_func = train_invertible_model
        config = {
                'n_blocks': tune.grid_search([2]),
                'n_depth': tune.grid_search([2]),
                'n_width': tune.grid_search([10]),
                'learning_rate': tune.grid_search([0.001]),
                'batch_size': tune.grid_search([16]),
                'datafile': args.datafile,
        }
    elif args.model == 'ASHA_invertible':
        train_func = train_invertible_model
        config = {
                'n_blocks': tune.randint(3,6),
                'n_depth': tune.randint(3,8),
                'n_width': tune.randint(10, 40),
                'learning_rate': tune.uniform(0.001, 0.0001),
                'batch_size': tune.choice([8,16,32,64]),
                'datafile': args.datafile,
        }
        # AsyncHyperBand enables aggressive early stopping of bad trials.
        """ Note: During training of the inverse model, it reports to ray every
            epoch. Therefore in the case of the maximum number of
            iterations, which is 30, 30 epochs have beeen gone through.
            (@ Romana: I am unsure, wether this is intended,
            but I have adapted the code below acordingly)
        """
        scheduler = AsyncHyperBandScheduler(
            # 'training_iteration' is incremented every time `trainable.step` is called
            time_attr='training_iteration',
            # The training result objective value attribute. Stopping procedures will use this attribute.
            metric='MAE_val',
            # mode: {min, max}. Determines whether objective is minimizing or maximizing the metric attribute.
            mode='min',
            # max time units per trial. Trials will be stopped after max_t time units (determined by time_attr).
            max_t=30,
            # Only stop trials at least this old in time. The units are the same as the attribute named by time_attr.
            grace_period=5,
            # Used to set halving rate and amount. This is simply a unit-less scalar.
            reduction_factor=4,
            # Number of brackets. Each bracket has a different halving rate, specified by the reduction factor.
            brackets=1
            )
        # 'training_iteration' is incremented every time `trainable.step` is called
        # For each trial, stop when trial has reached 30 iterations
        stopping_criteria = {"training_iteration": 30}
    else:
        raise ValueError(f'Unknown model: {args.model}')

    # Tune will automatically run parallel trials across all available
    # cores/GPUs on your machine or cluster. To limit the number of cores that
    # Tune uses, you can call ray.init(num_cpus=<int>, num_gpus=<int>) before
    # tune.run.
    ray.init()
    # Use Async Successive Halving, https://arxiv.org/abs/1810.05934
    if 'ASHA' in args.model:
        start = time.time()
        analysis = tune.run(train_func,
                            # num_samples are number of trials.
                            # Each trial is one instance of a Trainable.
                            num_samples=80,  # 256 took about 8.5h for the forward model
                            name=args.model + '_id',
                            config=config,
                            local_dir=result_dir,
                            scheduler=scheduler,
                            stop=stopping_criteria,
                            # 1 GPU has 11 GB, 16 cores, I estimate each run to
                            # consume about 4.296875 GB
                            resources_per_trial={'cpu': 2,
                                                 'gpu': 0.5},
                            queue_trials=False)
    else:  # No ASHA, plain vanilla
        start = time.time()
        analysis = tune.run(train_func,
                            # num_samples are number of trials.
                            # Each trial is one instance of a Trainable.
                            num_samples=1,
                            name=args.model + '_id',
                            config=config,
                            local_dir=result_dir,
                            # Tune will allocate the specified GPU and CPU
                            # ``resources_per_trial`` to each individual trial
                            # (defaulting to 1 CPU per trial). Under the hood,
                            # Tune runs each trial as a Ray actor, using Ray's
                            # resource handling to allocate resources and place
                            # actors. A trial will not be scheduled unless at
                            # least that amount of resources is available in the
                            # cluster, preventing the cluster from being
                            # overloaded.
                            resources_per_trial={'cpu': 20,
                                                 'gpu': 4},
                            queue_trials=False)

    # Save the training histories and the results.
    for key, df in analysis.trial_dataframes.items():
        ID = df['trial_id'].unique()[0]
        df.to_csv(f'{history_dir}/{ID}.csv')

    analysis.dataframe().to_csv(f'{result_dir}/results.csv')

    end = time.time()
    with open(f'{result_dir}/time_for_FW_scan.txt', 'w') as file:
        file.write(f'{end - start} s')
