import os
import time
import argparse
import ray
from ray import tune
import numpy as np
from helper_functions.xgboost_scan_helper_functions_configs import train_xgboost_model


if __name__ == '__main__':
    # Parse the command line arguments.
    parser = argparse.ArgumentParser()
    parser.add_argument('--result_dir',
                        help='Directory in which to save the results.')
    parser.add_argument('--datafile',
                        help='Path to the HDF5 file containing the dataset.')
    args = parser.parse_args()

    result_dir = args.result_dir
    history_dir = f'{result_dir}/histories'

    for directory in [result_dir, history_dir]:
        if not os.path.exists(directory):
            os.makedirs(directory)
            
    train_func = train_xgboost_model
    config = {
        'datafile': args.datafile,
        'early_stopping_rounds': 50,
        'max_trees': 7000,  # Equivalent to epochs
        'max_depth': tune.grid_search([4,6,8]),
        'learning_rate': tune.uniform(1e-3, 1e-1),
        'subsample': tune.uniform(0.5, 1.0),
        'colsample': tune.uniform(0.5, 1.0),
        'validation_split': 0.2,
    }


    ray.init(_temp_dir= '/scratch/ray')

    start = time.time()
    analysis = tune.run(train_func,
                        # num_samples are number of trials.
                        # Each trial is one instance of a Trainable.
                        # If grid_search, each grid point is repeated num_samples times
                        num_samples = 5,
                        name = 'xgboost_id',
                        config = config,
                        local_dir = result_dir,
                        resources_per_trial = {'cpu': 1, 'gpu': 0},
                        queue_trials = False)

    # Save the training histories and the results.
    for key, df in analysis.trial_dataframes.items():
        ID = df['trial_id'].unique()[0]
        df.to_csv(f'{history_dir}/{ID}.csv')

    analysis.dataframe().to_csv(f'{result_dir}/results.csv')

    end = time.time()
    with open(f'{result_dir}/time_for_FW_scan.txt', 'w') as file:
        file.write(f'{end - start} s')
    with open(f'{result_dir}/path_to_datafile.txt', 'w') as file:
        file.write(args.datafile)
