import setuptools

setuptools.setup(
    name='aerosol_retrieval',
    version='0.1',
    author='Romana Boiger',
    description='Code for "Aerosol Retrieval"',
    url='https://gitlab.psi.ch/boiger_r/Aerosol_NN',
    packages=setuptools.find_packages(),
    python_requires='>=3.7',
)
